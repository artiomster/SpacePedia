﻿using System;
using System.Collections.Generic;
using RichHudFramework.UI;
using Spacepedia.XMLClasses;
using VRageMath;
using EventHandler = RichHudFramework.EventHandler;

namespace Spacepedia
{
    public sealed class ChangeLogList : HudElementBase
    {
        private readonly string ModName;
        public readonly ListBox<HudElementBase> scrollbox;
        public event EventHandler SelectionChanged;
        public HudElementBase SelectedInPage;
        public string SelectedName;
        private readonly BorderedButton ButtonP, ButtonN;
        private readonly BorderBox listBorder;
        private readonly Dictionary<HudNodeBase,NewHighlightBox> HighlightBoxLinks = new Dictionary<HudNodeBase, NewHighlightBox>();
        private readonly Dictionary<Label, string> LinksOfPages = new Dictionary<Label, string>();

        public void Add(List<Page> list)
        {
            foreach (var changes in list)
            {
                var Highlight = new NewHighlightBox
                {
                    DimAlignment = DimAlignments.Both,
                    AlignVertical = false,
                    Padding = new Vector2(8f, 0f),
                    Visible = !XMLParse.History.isChangesRead(ModName, changes.UniqueId)
                };
                var Box = new BorderBox
                {
                    DimAlignment = DimAlignments.Both,
                    Color = TerminalFormatting.LimedSpruce,
                    Padding = new Vector2(8f, 0f),
                    Thickness = 1f,
                };
                var Background = new TexturedBox
                {
                    DimAlignment = DimAlignments.Both,
                    Color = new Color(42, 55, 62),
                    Padding = new Vector2(8f, 0f),
                };

                Background.ZOffset -= 1;
                
                HudElementBase Page = null;
                if (changes is ChangesPlainPage) Page = new PlainPageChangesDisplay(changes);
                if (changes is ChangesMeshPage) Page = new MeshPageChangesDisplay(changes);
                if (Page == null) return;
                
                var PageName = new RichText(changes.PageName, new GlyphFormat(alignment: TextAlignment.Center, color: Color.White)); 
                var entry = scrollbox.Add(PageName, Page); 
                
                entry.Element.RegisterChild(Box);
                entry.Element.RegisterChild(Background);
                entry.Element.RegisterChild(Highlight);
                
                LinksOfPages.Add(entry.Element, changes.Header == "" ? changes.PageName : changes.Header);
                HighlightBoxLinks.Add(entry.Element, Highlight);
            }
        }
        
        public ChangeLogList(string _ModName, HudParentBase parent = null) : base(parent)
        {
            ModName = _ModName;
            
            ButtonP = new BorderedButton(this)
            {
                ParentAlignment = ParentAlignments.Left | ParentAlignments.Center | ParentAlignments.Inner,
                Text = SpacepediaMain.Translation.Previous,
                Size = new Vector2(150f,this.Height),
                Padding = new Vector2(0f,0f),
                AutoResize = false
            };
            ButtonP.MouseInput.LeftClicked += (sender, args) => HandleButtonPress(false);
            
            listBorder = new BorderBox(ButtonP)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.Center | ParentAlignments.InnerV,
                Color = TerminalFormatting.LimedSpruce,
                Thickness = 0f,
            };

            scrollbox = new ListBox<HudElementBase>(listBorder)
            {
                SizingMode = HudChainSizingModes.FitMembersOffAxis | HudChainSizingModes.ClampChainOffAxis,
                ParentAlignment = ParentAlignments.Left | ParentAlignments.Top | ParentAlignments.Inner,
                Color = Color.Transparent,
                DimAlignment = DimAlignments.Both,
                Padding = new Vector2(20f, 0f),
                MinVisibleCount = 1,
                AlignVertical = false
            };
            
            scrollbox.SelectionChanged += HandleSelectionChange;

            ButtonN = new BorderedButton(listBorder)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.Center | ParentAlignments.InnerV,
                Text = SpacepediaMain.Translation.Next,
                Size = new Vector2(150f,this.Height),
                Padding = new Vector2(0f,0f),
                AutoResize = false
            };
            ButtonN.MouseInput.LeftClicked += (sender, args) => HandleButtonPress(true);
        }

        protected override void Layout()
        {
            listBorder.Width = Width - ButtonP.Width - ButtonN.Width;
            listBorder.Height = Height;
            ButtonP.Height = Height / 2;
            ButtonN.Height = ButtonP.Height;
        }

        private void HandleButtonPress(bool next)
        {
            var index = next ? scrollbox.SelectionIndex + 1 : scrollbox.SelectionIndex - 1;
            scrollbox.SetSelectionAt(index);
            if (next) scrollbox.hudChain.End++;
            else scrollbox.hudChain.Start--;
        }

        private void HandleSelectionChange(object page, EventArgs e)
        {
            if(page != null) SelectedInPage = scrollbox.Selection.AssocMember;
            
            var PageName = scrollbox.Selection.Element.Text.ToString();
            XMLParse.History.SetChangesRead(ModName, PageName, true);
            XMLParse.UpdateH();

            var Element = scrollbox.Selection.Element;
            Element.RemoveChild(HighlightBoxLinks[Element]);
            SelectedName = LinksOfPages[Element];

            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}