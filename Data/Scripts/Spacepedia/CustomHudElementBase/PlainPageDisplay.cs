﻿﻿using System;
using RichHudFramework.UI;
using RichHudFramework.UI.Rendering;
using Spacepedia.XMLClasses;
using VRageMath;

namespace Spacepedia
{

    public class  PlainPageDisplay : PlainPageDisplayBase
    {
        public PlainPageDisplay(PlainPage page, HudParentBase parent = null) : base(page, parent)
        { }
        public PlainPageDisplay(Page page, HudParentBase parent = null) : base(page as PlainPage, parent)
        { }
    }
    
    public class  PlainPageChangesDisplay : PlainPageDisplayBase
    {
        public PlainPageChangesDisplay(Page page, HudParentBase parent = null) : base(new PlainPage(page as ChangesPlainPage), parent)
        { }
    }
   
    public class PlainPageDisplayBase : HudElementBase
    {
        private readonly LabelBox LabelBox;
        public RichText Text { get { return LabelBox.Text; } set { LabelBox.Text = value; } }
        
        private readonly ScrollBar verticalScroll;

        protected PlainPageDisplayBase(PlainPage Page, HudParentBase parent = null) : base(parent)
        {
            if(Page == null) return;
            
            LabelBox = new LabelBox(this)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
                BuilderMode = TextBuilderModes.Wrapped,
                DimAlignment = DimAlignments.Height,
                AutoResize = false,
                Format = new GlyphFormat(Page.TextColor, textSize:Page.TextSize),
                VertCenterText = false,
                Color = SpacepediaMain.LabelColor,
            };
            
            verticalScroll = new ScrollBar(this)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.InnerH,
                DimAlignment = DimAlignments.Height | DimAlignments.IgnorePadding,
                Offset = new Vector2(15f,0f),
                Vertical = true,
            };

            // ReSharper disable once UnusedVariable
            var scrollDivider = new TexturedBox(LabelBox)
            {
                ParentAlignment = ParentAlignments.Right,
                DimAlignment = DimAlignments.Height,
                Padding = new Vector2(15f,15f),
                Color = SpacepediaMain.BorderColor,
                Width = 1f,
            };
            
            Text = Page.IsSmart ? TextFormatting.GetFormattedText(Page) : Page.Text;
            
            UseCursor = true;
            ShareCursor = true;
        }
        
        protected override void Layout()
        {
            LabelBox.Width = Width - verticalScroll.slide.Width - verticalScroll.Offset.X;
            
            ITextBoard textBoard = LabelBox.TextBoard;
            verticalScroll.slide.SliderHeight = (textBoard.Size.Y / textBoard.TextSize.Y) * verticalScroll.Height;
        }
        
        protected override void HandleInput(Vector2 cursorPos)
        {
            base.HandleInput(cursorPos);
            ITextBoard textBoard = LabelBox.TextBoard;
            IMouseInput vertControl = verticalScroll.slide.MouseInput;
            verticalScroll.Max = Math.Max(0f, textBoard .TextSize.Y - textBoard.Size.Y);
            if (!vertControl.IsLeftClicked) verticalScroll.Current = textBoard.TextOffset.Y;
            if (IsMousedOver || verticalScroll.IsMousedOver)
            {
                if (SharedBinds.MousewheelUp.IsPressed) verticalScroll.Current -= SpacepediaMain.MouseScrollOffset;
                else if (SharedBinds.MousewheelDown.IsPressed) verticalScroll.Current += SpacepediaMain.MouseScrollOffset;
            }
            textBoard .TextOffset = new Vector2(0, verticalScroll.Current);
        }
    }
}