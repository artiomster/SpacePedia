﻿﻿using VRageMath;
using RichHudFramework.UI;
using RichHudFramework;
using RichHudFramework.UI.Client;

 namespace Spacepedia
{
    public sealed class NewChangesAsk : WindowBase
    {
        private readonly HudChain bodyChain;
        private readonly BorderBox ButtonsBox;
        private readonly TexturedBox TopDivider;

        public NewChangesAsk(HudParentBase parent = null) : base(parent)
        {
            HeaderBuilder.Format = TerminalFormatting.HeaderFormat;
            HeaderBuilder.SetText("New Changes, do you want to see?");
            
            BorderColor = new Color(84, 98, 107);
            
            MinimumSize = new Vector2(400f, 100f);
            
            TopDivider = new TexturedBox()
            {
                Color = new Color(53, 66, 75),
                DimAlignment = DimAlignments.Width,
                Height = 1f,
            };
            var bottomDivider = new TexturedBox()
            {
                Color = new Color(53, 66, 75),
                DimAlignment = DimAlignments.Width,
                Height = 1f,
            };
            ButtonsBox = new BorderBox()
            {
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Inner,
                Color = TerminalFormatting.LimedSpruce,
                Size = new Vector2(380f,50f),
                Thickness = 0f,
            };
            var ButtonYes = new BorderedButton(ButtonsBox)
            {
                ParentAlignment = ParentAlignments.Left | ParentAlignments.Center | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Height,
                Text = "Yes",
                Width = 180f,
                AutoResize = false
            };
            ButtonYes.MouseInput.LeftClicked += (sender, args) => HandleButtonPress(true);
            
            var ButtonNo = new BorderedButton(ButtonsBox)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.Center | ParentAlignments.Inner,
                Text = "No",
                DimAlignment = DimAlignments.Height,
                Width = 180f,
                AutoResize = false
            };
            ButtonNo.MouseInput.LeftClicked += (sender, args) => HandleButtonPress(false);
            
            bodyChain = new HudChain(true, this)
            {
                SizingMode = HudChainSizingModes.FitChainAlignAxis | HudChainSizingModes.ClampChainBoth,
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Width,
                Spacing = 10f,
                CollectionContainer = {TopDivider,ButtonsBox, bottomDivider},
            };
        }
        private void HandleButtonPress(bool isYes)
        {
            Visible = false;
            HudMain.EnableCursor = false;
            if (isYes) SpacepediaMain.SpacepediaToggle(null,null);
        }

        protected override void Layout()
        {
            base.Layout();
            
            header.Height = 50f;
            Size = new Vector2(bodyChain.Width,header.Height + ButtonsBox.Height + TopDivider.Height + bodyChain.Spacing * 2);

            // Bound window offset to keep it from being moved off screen
            Vector2 min = new Vector2(HudMain.ScreenWidth, HudMain.ScreenHeight) / (HudMain.ResScale * -2f), max = -min;
            Offset = Vector2.Clamp(Offset, min, max);
            
            // Update color opacity
            BodyColor = TerminalFormatting.OuterSpace.SetAlphaPct(HudMain.UiBkOpacity);

            header.Color = BodyColor;
            
            /////////////////////////////////////////////////////////////
            Padding = new Vector2(10,0f);
        }
    }
}
