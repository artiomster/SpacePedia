using RichHudFramework.UI;
using RichHudFramework.UI.Rendering;
using VRageMath;

namespace Spacepedia
{
    public class NewHighlightBox : HudElementBase
    {
        public Material Material
        {
            get { return tabBoard.Material; }
            set
            {
                tabBoard.Material = value;
                textureBox.Material = value;
                borderBox.Material = value;
            }
        }

        public MaterialAlignment MatAlignment
        {
            get { return tabBoard.MatAlignment; }
            set
            {
                tabBoard.MatAlignment = value;
                textureBox.MatAlignment = value;
                borderBox.MatAlignment = value;
            }
        }

        public Color Color
        {
            get { return tabBoard.Color; }
            set
            {
                tabBoard.Color = value;
                borderBox.Color = value;
                textureBox.Color = value;
            }
        }

        public override Vector2 Padding
        {
            get { return borderBox.Padding; }
            set
            {
                borderBox.Padding = value;
                textureBox.Padding = value;
            }
        }

        public float Thickness
        {
            get { return borderBox.Thickness; }
            set { borderBox.Thickness = value; }
        }

        private readonly TexturedBox textureBox = new TexturedBox();
        private readonly BorderBox borderBox;
        private readonly MatBoard tabBoard;
        public bool AlignVertical = true;


        public NewHighlightBox(HudParentBase parent = null) : base(parent)
        {
            tabBoard = new MatBoard {Color = Color.Gold};
            borderBox = new BorderBox(this)
            {
                Color = Color.Gold,
                DimAlignment = DimAlignments.Both,
            };

        }

        protected override void Draw()
        {
            base.Draw();
            if (tabBoard.Color.A > 0)
            {
                CroppedBox box = default(CroppedBox);
                Vector2 tabPos = cachedPosition, tabSize;
                if (AlignVertical)
                {
                    tabSize = new Vector2(4f, Size.Y - cachedPadding.Y);
                    tabPos.X += (-Size.X + tabSize.X) * .5f;
                    tabSize *= .5f;
                }
                else
                {
                    tabSize = new Vector2(Size.X - cachedPadding.X, 4f);
                    tabPos.Y += (-Size.Y + tabSize.Y) * .5f;
                    tabSize *= .5f;
                }

                box.bounds = new BoundingBox2(tabPos - tabSize, tabPos + tabSize);
                box.mask = maskingBox;
                tabBoard.Draw(ref box, HudSpace.PlaneToWorldRef);
            }
        }
    }
}