﻿﻿using System;
using MIG.Shared.SE;

namespace Spacepedia
{
    public class Hooks
    {
        private static readonly Action<string, string, bool> openIfUnread = (TreeId, PageId, isForced) => 
            SpacepediaMain.SpacepediaWindow.OpenPage(TreeId, PageId, isForced);
        private static readonly Action openIntro = () => 
            SpacepediaMain.SpacepediaWindow.OpenIntro();
        
        public static void Init()
        {
            ModConnection.Init(38503,38504, "Spacepedia");
            ModConnection.SetValue("Spacepedia.OpenIfUnread", openIfUnread);
            ModConnection.SetValue("Spacepedia.OpenIntro", openIntro);
        }
        
        public static void Close() => ModConnection.Close();
    }
}