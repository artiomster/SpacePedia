using System;
using System.Collections.Generic;
using System.Linq;
using RichHudFramework.UI;
using RichHudFramework.UI.Client;
using RichHudFramework.UI.Rendering;
using Spacepedia.XMLClasses;
using VRage.Utils;
using VRageMath;
using EventHandler = RichHudFramework.EventHandler;

namespace Spacepedia
{
    public class SpacepediaList : HudElementBase
    {
        private static readonly Material arrowButtonMat = new Material("RichHudRightArrow", new Vector2(40f));
        private readonly Dictionary<string, TreeList<HudElementBase>> Trees = new Dictionary<string, TreeList<HudElementBase>>();
        private readonly Dictionary<Label, KeyValuePair<string,string>> LinksOfPages = new Dictionary<Label,KeyValuePair<string,string>>();
        private readonly Dictionary<Label ,NewHighlightBox> HighlightLinks = new Dictionary<Label, NewHighlightBox>();
        
        private readonly HudChain Chain;
        public HudElementBase SelectedPage;
        public event EventHandler SelectionChanged;

        public void Add(Tree inTree, SpacepediaPage Page)
        {
            if (Trees.ContainsKey(inTree.TreeId)) 
                Trees[inTree.TreeId].Add(Page.PageName, Page);
            else
            {
                var TreeList = new TreeList<HudElementBase> {Name = inTree.TreeName, Width = 270f};
                TreeList.Add(Page.PageName, Page);
                
                TreeList.ParentAlignment = ParentAlignments.Top | ParentAlignments.Inner;
                TreeList.Width = Width;
                TreeList.SelectionChanged += (sender, args) => HandleSelectionChange(TreeList);
                Trees.Add(inTree.TreeId, TreeList);
                Chain.Add(TreeList);
            }
            
        }
        
        public void Add(Tree inTree, Page inPage, bool isIntro = false)
        {
            var Highlight = new NewHighlightBox
            {
                DimAlignment = DimAlignments.Both,
                Visible = !isIntro && !XMLParse.History.isWikiPRead(inTree.TreeId, inPage.UniqueId),
            };

            var Page = new SpacepediaPage(inPage);
            
            if (Trees.ContainsKey(inTree.TreeId))
            {
                var entry = Trees[inTree.TreeId].Add(inPage.PageName, Page);
                if (inTree.TreeName != "BUGS")
                {
                    entry.Element.RegisterChild(Highlight);
                    LinksOfPages.Add(entry.Element, new KeyValuePair<string, string>(inTree.TreeId, inPage.UniqueId));
                    HighlightLinks.Add(entry.Element, Highlight);
                }
            }
            else
            {
                var TreeList = new TreeList<HudElementBase> {Name = inTree.TreeName, Width = 270f};
                var entry = TreeList.Add(inPage.PageName, Page);
                
                if(inTree.TreeName != "BUGS")
                {
                    entry.Element.RegisterChild(Highlight);
                    LinksOfPages.Add(entry.Element, new KeyValuePair<string, string>(inTree.TreeId, inPage.UniqueId));
                    HighlightLinks.Add(entry.Element,Highlight);
                }
                
                TreeList.ParentAlignment = ParentAlignments.Top | ParentAlignments.Inner;
                TreeList.Width = Width;
                TreeList.SelectionChanged += (sender, args) => HandleSelectionChange(TreeList, isIntro: isIntro);
                Trees.Add(inTree.TreeId, TreeList);
                if(isIntro) Chain.Insert(0,TreeList);
                else Chain.Add(TreeList);
            }
        }

        public void AddRange(Dictionary<Tree, List<Page>> list)
        {
            foreach (var Tree in list)
            {
                foreach (var Page in Tree.Value)
                {
                    Add(Tree.Key, Page);
                }
            }
        }
        
        public void AddChangeLogs(string ModName, Dictionary<string, ChangeLogsXML> ModList)
        {
            if(ModList.Count == 0) return;
            if (Trees.ContainsKey(ModName))
            {
                foreach (var Mod in ModList)
                {
                    var Highlight = new NewHighlightBox
                    {
                        DimAlignment = DimAlignments.Both,
                        Visible = !XMLParse.History.isAllChangesRead(Mod.Key)
                    };
                    
                    var entry = Trees[ModName].Add(Mod.Key, new ChangeLogPage(Mod.Key, Mod.Value));
                    entry.Element.RegisterChild(Highlight);
                    HighlightLinks.Add(entry.Element,Highlight);
                }
            }
            else
            {
                var TreeList = new TreeList<HudElementBase> {Name = ModName, Width = 270f};
                foreach (var Mod in ModList)
                {
                    var Highlight = new NewHighlightBox
                    {
                        DimAlignment = DimAlignments.Both,
                        Visible = !XMLParse.History.isAllChangesRead(Mod.Key)
                    };
                    var Page = new ChangeLogPage(Mod.Key, Mod.Value);
                    Page.HSCI += (sender, args) => UpdateHighlight(TreeList);
                    
                    var entry = TreeList.Add(Mod.Key, Page);
                    entry.Element.RegisterChild(Highlight);
                    HighlightLinks.Add(entry.Element,Highlight);
                }
                TreeList.ParentAlignment = ParentAlignments.Top | ParentAlignments.Inner;
                TreeList.Width = Width;
                
                TreeList.SelectionChanged += (sender, args) => HandleSelectionChange(TreeList, true);
                Trees.Add(ModName,TreeList);
                Chain.Add(TreeList);
            }
        }

        private void SetOthersToDefault(IReadOnlyHudElement Tree)
        {
            foreach (var container in Chain.CollectionContainer)
            {
                var otherTree = container.Element as TreeList<HudElementBase>;
                if (otherTree != null && otherTree != Tree)
                {
                    otherTree.ClearSelection();
                }
            }
        }
        
        private void HandleSelectionChange(TreeList<HudElementBase> List, bool isChangeLogs = false, bool isIntro = false)
        {
            if (List != null)
            {
                SelectedPage = List.Selection.AssocMember;
                
                var ListName = List.Name.ToString();
                if (ListName != "BUGS" && !isChangeLogs && !isIntro)
                {
                    var element = List.Selection.Element;
                    var PageLink = LinksOfPages[element];
                    XMLParse.History.SetWikiPRead(PageLink.Key, PageLink.Value, true);
                    element.RemoveChild(HighlightLinks[element]);
                    XMLParse.UpdateH();
                }

                if (isChangeLogs)
                {
                    var Page = List.Selection.AssocMember as ChangeLogPage;
                    if (Page?._ChangeLogList.scrollbox.SelectionIndex == -1) 
                        Page._ChangeLogList.scrollbox.SetSelectionAt(0);
                }
                
                SetOthersToDefault(List);
            }
            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateHighlight(TreeList<HudElementBase> List)
        {
            var element = List.Selection.Element;
            element.RemoveChild(HighlightLinks[element]);
        }
        
        private void HandleListHideSShow()
        {
            Chain.Visible = !Chain.Visible;
            verticalScroll.Visible = Chain.Visible;
            Width = Chain.Visible ? 270f : 0f;
        }

        public void OpenIntro()
        {
            var Tree = Chain.CollectionContainer.First(x => (x.Element as TreeList<HudElementBase>)?.Name.ToString() == XMLParse.Intro.ListName);

            var TreeList = Tree.Element as TreeList<HudElementBase>;
            if (TreeList == null) return;
            TreeList.ListContainer.OpenList();
            foreach (var child2 in TreeList.ListContainer.EntryList)
            {
                if (child2.Element.Text.ToString() == XMLParse.Intro.PageName)
                {
                    TreeList.SetSelection(child2);
                    return;
                }
            }
        }

        public void OpenSelection(string TreeId, string PageId)
        {
            var Tree = XMLParse.GetTreeById(TreeId);
            if (Tree == null) return;
            var Page = XMLParse.GetPageById(Tree, PageId);
            if (Page == null) return;

            var _child = Chain.CollectionContainer.First(x => (x.Element as TreeList<HudElementBase>)?.Name.ToString() == Tree.TreeName);

            var child = _child.Element as TreeList<HudElementBase>;

            if (child == null) return;
            
            
            child.ListContainer.OpenList();
            foreach (var child2 in child.ListContainer.EntryList)
            {
                if (child2.Element.Text.ToString() == Page.PageName)
                {
                    SpacepediaMain.SpacepediaWindow.Visible = true;
                    HudMain.EnableCursor = true;
                    child.SetSelection(child2);
                    return;
                }
            }
        }

        private readonly ScrollBar verticalScroll;
        private readonly BorderBox listBorder;
        private readonly Button listButton;

        public SpacepediaList(HudParentBase parent = null) : base(parent)
        {
            listBorder = new BorderBox(this)
            {
                DimAlignment = DimAlignments.Both,
                Thickness = 1f,
                Color = new Color(53, 66, 75),
                IsMasking = true,
            };
            
            Chain = new HudChain(true, listBorder)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
            };
            
            verticalScroll = new ScrollBar(this)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.InnerV | ParentAlignments.Bottom,
                Padding = new Vector2(0f,0f),
                Offset = new Vector2(5f,0f),
                Vertical = true,
            };
            
            
            listButton = new Button(this)
            {
                Material = arrowButtonMat,
                HighlightColor = Color.White,
                ParentAlignment =  ParentAlignments.Right | ParentAlignments.Top | ParentAlignments.InnerV,
                Size = new Vector2(40f),
                Offset = new Vector2(-8f,0),
                Color = new Color(173, 182, 189),
            };
            listButton.MouseInput.LeftClicked += (sender, args) => HandleListHideSShow();
            
            UseCursor = true;
            ShareCursor = true;
        }
        
        protected override void Layout()
        {
            verticalScroll.slide.SliderHeight = (listBorder.Size.Y / Chain.Size.Y) * verticalScroll.Height;
            verticalScroll.Height = Height - listButton.Size.Y;
        }

        protected override void HandleInput(Vector2 cursorPos)
        {
            base.HandleInput(cursorPos);
            IMouseInput vertControl = verticalScroll.slide.MouseInput;
            verticalScroll.Max = Math.Max(0f, Chain.Size.Y - listBorder.Size.Y);
            if (!vertControl.IsLeftClicked) verticalScroll.Current = Chain.Offset.Y;
            if (IsMousedOver || verticalScroll.IsMousedOver)
            {
                if (SharedBinds.MousewheelUp.IsPressed) verticalScroll.Current -= SpacepediaMain.MouseScrollOffset;
                else if (SharedBinds.MousewheelDown.IsPressed) verticalScroll.Current += SpacepediaMain.MouseScrollOffset;
            }
            Chain.Offset = new Vector2(0, verticalScroll.Current);
        }
    }
}