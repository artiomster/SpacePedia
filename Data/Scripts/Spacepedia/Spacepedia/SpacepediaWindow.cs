﻿using System;
using System.Collections.Generic;
using VRageMath;
using RichHudFramework.UI.Rendering;
using RichHudFramework.UI;
using RichHudFramework;
using RichHudFramework.Client;
using RichHudFramework.UI.Client;
using Sandbox.Game;
using Spacepedia.XMLClasses;
using VRage.Utils;

namespace Spacepedia
{
    /// <summary>
    /// Example Text Editor window
    /// </summary>
    public sealed class SpacepediaWindow : WindowBase
    {
        private readonly SpacepediaList SpacepediaList;
        private HudElementBase SelectedPage => SpacepediaList.SelectedPage;
        private HudElementBase lastPage;

        private readonly HudChain bodyChain;
        private readonly TexturedBox topDivider, bottomDivider;
        private readonly SliderBox TransparencySlider;
        private readonly NamedCheckBox TransparencyCheckBox;
        
        private static readonly Material closeButtonMat = new Material("RichHudCloseButton", new Vector2(32f));

        private static readonly Dictionary<string, SpacepediaPage> Bugs = new Dictionary<string, SpacepediaPage>();
        private static readonly Dictionary<string, PlainPage> BugsToLoad = new Dictionary<string, PlainPage>();
        
        public SpacepediaWindow(HudParentBase parent = null) : base(parent)
        {
            HeaderBuilder.Format = TerminalFormatting.HeaderFormat;
            HeaderBuilder.SetText("Spacepedia");
            
            BodyColor = new Color(37, 46, 53);
            BorderColor = new Color(84, 98, 107);

            Offset = XMLParse.settings.dimension.PositionOffset;
            MinimumSize = XMLParse.settings.dimension.MinSize;
            Size = XMLParse.settings.dimension.Size;

            topDivider = new TexturedBox(header)
            {
                ParentAlignment = ParentAlignments.Bottom,
                DimAlignment = DimAlignments.Width,
                Padding = new Vector2(80f, 0f),
                Color = new Color(53, 66, 75),
                Height = 1f,
            };
            
            var middleDivider = new TexturedBox()
            {
                Padding = new Vector2(24f, 0f),
                Color = new Color(53, 66, 75),
                Width = 26f,
            };
            
            bottomDivider = new TexturedBox(this)
            {
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.InnerV,
                DimAlignment = DimAlignments.Width,
                Offset = new Vector2(0f, 40f),
                Padding = new Vector2(80f, 0f),
                Color = new Color(53, 66, 75),
                Height = 1f,
            };
            
            SpacepediaList = new SpacepediaList {Width = 270f};
            SpacepediaList.SelectionChanged += (sender, args) => HandleSelectionChange();

            bodyChain = new HudChain(false, topDivider)
            {
                SizingMode = HudChainSizingModes.FitMembersOffAxis | HudChainSizingModes.ClampChainBoth,
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Left | ParentAlignments.InnerH,
                Padding = new Vector2(80f, 40f),
                Spacing = 12f,
                CollectionContainer = { SpacepediaList, middleDivider },
            };
            
            UpdateBugs += addBugsToList;
            SpacepediaMain.Networking.GotIntro += AddIntro;
            
            SpacepediaList.AddRange(XMLParse.WikiPages);
            SpacepediaList.AddChangeLogs("Change Logs", XMLParse.AllModsChanges);
            
            var closeButton = new Button(header)
            {
                Material = closeButtonMat,
                HighlightColor = Color.White,
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Right | ParentAlignments.Inner,
                Size = new Vector2(30f),
                Offset = new Vector2(-18f, -14f),
                Color = new Color(173, 182, 189),
            };
            closeButton.MouseInput.LeftClicked += SpacepediaMain.SpacepediaToggle;
            
            TransparencySlider = new SliderBox(closeButton)
            {
                ParentAlignment = ParentAlignments.Left | ParentAlignments.InnerV,
                Size = new Vector2(150f,35f),
                Offset = new Vector2(-15f,0f),
                UseCursor = true,
                Percent = XMLParse.settings.transparency.Percentage,
            };
            TransparencySlider.MouseInput.LeftReleased += (o, e) => UpdateSettings();
            
            TransparencyCheckBox = new NamedCheckBox(TransparencySlider)
            {
                Name = SpacepediaMain.Translation.TransparencyCheckBox,
                ParentAlignment = ParentAlignments.Left | ParentAlignments.InnerV,
                Offset = new Vector2(15f,0f),
                IsBoxChecked =  XMLParse.settings.transparency.Auto,
                InputEnabled = true,
            };
            TransparencyCheckBox.MouseInput.LeftReleased += (o, e) => UpdateSettings();
            
            foreach (var BugPage in Bugs)
            {
                if (!BugPage.Value.Registered) addBugsToList(BugPage.Value);
            }
            foreach (var BugPage in BugsToLoad)
            {
                var Page = new SpacepediaPage(BugPage.Value);
                Bugs.Add(BugPage.Key, Page);
                UpdateBugs?.Invoke(Page);
            }

            UpdateSettings(false);
            MouseInput.LeftReleased += (o, e) => UpdateSettings();
        }

        private void UpdateSettings(bool save = true)
        {
            XMLParse.settings.dimension.Size = Size;
            XMLParse.settings.dimension.PositionOffset = Offset;
            TransparencySlider.MouseInput.InputEnabled = !TransparencyCheckBox.IsBoxChecked;
            
            XMLParse.settings.transparency.Auto = TransparencyCheckBox.IsBoxChecked;
            XMLParse.settings.transparency.Percentage = TransparencySlider.Percent;
            if (save) XMLParse.SaveSettings();
        }
        
        public void ResetSettings()
        {
            XMLParse.settings = new SettingsXML();
            
            Size = XMLParse.settings.dimension.Size;
            Offset = new Vector2(0,0);
            
            TransparencyCheckBox.IsBoxChecked = XMLParse.settings.transparency.Auto ;
            TransparencySlider.Percent = XMLParse.settings.transparency.Percentage;
            
            TransparencySlider.MouseInput.InputEnabled = !TransparencyCheckBox.IsBoxChecked;
            
            XMLParse.SaveSettings();
        }
        
        private void AddIntro()
        {
            if (XMLParse.Intro.isNew()) return;
            SpacepediaList.Add(new Tree(XMLParse.Intro), new PlainPage(XMLParse.Intro), true);
        }

        public void OpenPage(string TreeId, string PageId, bool isForced = false)
        {
            if (isForced || !XMLParse.History.isWikiPRead(TreeId, PageId))
            {
                SpacepediaList.OpenSelection(TreeId, PageId);
            }
        }
        
        public void OpenPage(Tree TreeId, Page PageId, bool isForced = false)
        {
            OpenPage(TreeId.TreeId, PageId.UniqueId, isForced);
        }

        public void OpenIntro()
        {
            SpacepediaList.OpenIntro();
        }
        
        private void HandleSelectionChange()
        {
            if (lastPage != null)
            {
                var pageElement = lastPage;
                int index = bodyChain.FindIndex(x => x.Element == pageElement);

                bodyChain.RemoveAt(index);
            }

            if (SelectedPage != null) bodyChain.Add(SelectedPage);

            lastPage = SelectedPage;
        }

        protected override void Layout()
        {
            base.Layout();
            
            header.Height = 50f;
            Padding = new Vector2(80f, 40f);
            
            if (SelectedPage != null)
            {
                var pageElement = SelectedPage;
                pageElement.Width = Width - Padding.X * 1.5f - SpacepediaList.Width - bodyChain.Spacing;
            }
            bodyChain.Height = Height - header.Height - topDivider.Height - Padding.Y - bottomDivider.Height;

            // Bound window offset to keep it from being moved off screen
            var min = new Vector2(HudMain.ScreenWidth, HudMain.ScreenHeight) / (HudMain.ResScale * -2f);
            var max  = -min;

            Offset = Vector2.Clamp(Offset, min, max);

            // Update color opacity
            BodyColor = BodyColor.SetAlphaPct(TransparencyCheckBox.IsBoxChecked ? HudMain.UiBkOpacity : TransparencySlider.Percent);
            
            header.Color = BodyColor;
        }

        public void ClientReset()
        {
            UpdateBugs -= addBugsToList;
            SpacepediaMain.Networking.GotIntro -= AddIntro;
        }

        public static event Action<SpacepediaPage> UpdateBugs;
        
        public static void AddOrEditBugs(string key, string value)
        {
            MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia, XMLParse ---> " + value);
            value += "\n";
            if (Bugs.ContainsKey(key)) Bugs[key].Text += value;
            else
            {
                if(RichHudClient.Registered)
                {
                    var Page = new SpacepediaPage(new PlainPage(key, value));
                    Bugs.Add(key, Page);
                    UpdateBugs?.Invoke(Page);
                }
                else
                {
                    var Page = new PlainPage(key, value);
                    BugsToLoad.Add(key, Page);
                }
            }
        }
        
        private void addBugsToList(SpacepediaPage page)
        {
            SpacepediaList.Add(new Tree("BUGS","BUGS"), page);
            MyVisualScriptLogicProvider.ShowNotification(SpacepediaMain.Translation.NewBugMessage,2000);
        }
    }
}
