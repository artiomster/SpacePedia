﻿using System;
using ProtoBuf;
using Sandbox.Game;
using Sandbox.ModAPI;
using Scripts.Specials.Messaging;
using Spacepedia.XMLClasses;
using VRage.Game.ModAPI;

namespace Spacepedia
{
    [ProtoContract]
    public class SyncIntro
    {
        [ProtoMember(1)] public readonly string ListName;
        [ProtoMember(2)] public readonly string PageName;
        [ProtoMember(3)] public readonly string Header;
        [ProtoMember(4)] public readonly string Text;
        [ProtoMember(5)] public readonly string CheckBoxText;
        [ProtoMember(6)] public readonly string CheckBoxRedText;
        [ProtoMember(7)] public readonly string ButtonRedText;
        [ProtoMember(8)] public readonly float TextSize;
        [ProtoMember(9)] public readonly DateTime CreationTime;

        public SyncIntro(){}
        public SyncIntro(IntroXML intro)
        {
            CreationTime = intro.CreateTime;
            ListName = intro.ListName;
            PageName = intro.PageName;
            Header = intro.Header;
            Text = intro.Text;
            CheckBoxText = intro.CheckBoxText;
            CheckBoxRedText = intro.CheckBoxRedText;
            ButtonRedText = intro.ButtonRedText;
            TextSize = intro.TextSize;
        }

        public override string ToString()
        {
            return CreationTime + "\n" + ListName + "\n" + PageName + "\n" + Header + "\n" + Text + "\n" + TextSize + "\n" + CheckBoxText + "\n" + CheckBoxRedText + "\n" + ButtonRedText;
        }
    }

    [ProtoContract]
    public class SyncAskIntro
    {
        [ProtoMember(1)] public readonly string CultureName;
        
        public SyncAskIntro(){}
        public SyncAskIntro(string CultureName)
        {
            this.CultureName = CultureName;
        }
    }
    
    [ProtoContract]
    public class SyncAcceptDate
    {
        [ProtoMember(1)] public DateTime IntroCreationTime = DateTime.MinValue;
        [ProtoMember(2)] public DateTime dataTime = DateTime.Now;
        [ProtoMember(3)] public readonly bool isAccepted;
        [ProtoMember(4)] public readonly bool AskedList;
        [ProtoMember(5)] public readonly string ListOfAccepted;
        
        public SyncAcceptDate(){}
        public SyncAcceptDate(DateTime IntroCreationTime, bool isAccepted)
        {
            this.IntroCreationTime = IntroCreationTime;
            this.isAccepted = isAccepted;
        }
        public SyncAcceptDate(bool AskedList)
        {
            this.AskedList = AskedList;
        }
        public SyncAcceptDate(string ListOfAccepted)
        {
            this.ListOfAccepted = ListOfAccepted;
        }
    }
    
    public class Networking
    {
        private static ushort IntroChannelId;
        private static ushort AcceptedChannelId;
        public event Action GotIntro;
        public static event Action GotListOfAccepted;
        public static string ListOfAccepted;
        
        public Networking(ushort IntroChannelId, ushort AcceptedChannelId)
        {
            Networking.IntroChannelId = IntroChannelId;
            Networking.AcceptedChannelId = AcceptedChannelId;
        }

        public void ForceInvokeGotIntro()
        {
            GotIntro?.Invoke();
        }

        public void Register()
        {
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(IntroChannelId, ReceivedPacket);
            MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(AcceptedChannelId, Received2Packet);
        }
        
        public void Unregister()
        {
            MyAPIGateway.Multiplayer.UnregisterSecureMessageHandler(IntroChannelId, ReceivedPacket);
            MyAPIGateway.Multiplayer.UnregisterSecureMessageHandler(AcceptedChannelId, Received2Packet);
        }

        private void ReceivedPacket(ushort id, byte[] bytes, ulong SteamId, bool isFromServer)
        {
            if (isFromServer)
            {
                var packet = MyAPIGateway.Utilities.SerializeFromBinary<SyncIntro>(bytes);
                XMLParse.Intro.Update(packet);
                GotIntro?.Invoke();
            }
            else
            {
                var packet = MyAPIGateway.Utilities.SerializeFromBinary<SyncAskIntro>(bytes);
                SendIntro(SteamId, packet.CultureName);
            }
        }
        private void Received2Packet(ushort id, byte[] bytes, ulong SteamId, bool isFromServer)
        {
            var packet = MyAPIGateway.Utilities.SerializeFromBinary<SyncAcceptDate>(bytes);

            if (!isFromServer)
            {
                if (packet.AskedList)
                {
                    var IdentityId = MyAPIGateway.Players.TryGetIdentityId(SteamId);
                    var Player = Common.GetPlayer(IdentityId);

                    if (GotAccess(Player.PromoteLevel))
                    {
                        XMLParse.ReadAcceptances();
                        var message = MyAPIGateway.Utilities.SerializeToBinary(new SyncAcceptDate(XMLParse.ListOfAccepted.CashedString));
                        MyAPIGateway.Multiplayer.SendMessageTo(id, message, SteamId);
                    }
                }
                else
                {
                    XMLParse.UpdateLOA(packet.IntroCreationTime, SteamId, packet.dataTime, packet.isAccepted);
                }
            }
            else
            {
                if (packet.ListOfAccepted == null) return;
                ListOfAccepted = packet.ListOfAccepted;
                GotListOfAccepted?.Invoke();
            }
        }

        private void SendIntro(ulong SteamId, string CultureName)
        {
            var message = MyAPIGateway.Utilities.SerializeToBinary(new SyncIntro(XMLParse.GetIntroXML(CultureName)));
            MyAPIGateway.Multiplayer.SendMessageTo(IntroChannelId, message, SteamId);
        }
        public void AskIntro(string CultureName)
        {
            var message = MyAPIGateway.Utilities.SerializeToBinary(new SyncAskIntro(CultureName));
            MyAPIGateway.Multiplayer.SendMessageToServer(IntroChannelId, message);
        }
        
        public void PlayerAccepted(DateTime IntroCreationTime, bool isAccepted = false)
        {
            var message = MyAPIGateway.Utilities.SerializeToBinary(new SyncAcceptDate(IntroCreationTime, isAccepted));
            MyAPIGateway.Multiplayer.SendMessageToServer(AcceptedChannelId, message);
        }
        public static void AskListOfAccepted()
        {
            var playerPromoteLevel = MyAPIGateway.Session.Player.PromoteLevel;
            if (GotAccess(playerPromoteLevel))
            {
                var message = MyAPIGateway.Utilities.SerializeToBinary(new SyncAcceptDate(true));
                MyAPIGateway.Multiplayer.SendMessageToServer(AcceptedChannelId, message);
            }
            else
            {
                MyVisualScriptLogicProvider.ShowNotification(SpacepediaMain.Translation.NotAllowed,2000, "Red");
            }
        }

        private static bool GotAccess(MyPromoteLevel level)
        {
            return level == MyPromoteLevel.SpaceMaster || level == MyPromoteLevel.Admin || level == MyPromoteLevel.Owner;
        }
    }
}