﻿/*﻿using System;
using MIG.Shared.SE;

namespace Spacepedia
{
    public class SpacepediaHooks
    {
        private static Action<string, string, bool> openPage;
        private static Action openIntro;

        /// <summary>
        /// Must be inited in LoadData of MySessionComponentBase
        /// </summary>
        public static void Init()
        {
            ModConnection.Init(38503,38504, "Spacepedia");

            ModConnection.Subscribe<Action<string, string, bool>>("Spacepedia.OpenPage", (x) => { openPage = x;});
            ModConnection.Subscribe<Action>("Spacepedia.OpenIntro", (x) => { openIntro = x;});
        }

        public static void Close()
        {
            ModConnection.Close();
        }

        public static void OpenPage(string TreeId, string PageId, bool isForced = false)
        {
            openPage.Invoke(TreeId, PageId, isForced);
        }

        public static void OpenIntro() => openIntro.Invoke();
    }
}*/