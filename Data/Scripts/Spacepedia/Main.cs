using System;
using System.Globalization;
using System.Text.RegularExpressions;
using RichHudFramework.Client;
using RichHudFramework.UI.Client;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using Scripts.Specials.Messaging;
using Spacepedia.XMLClasses;
using VRage.Game;
using VRage.Game.Components;
using VRageMath;

namespace Spacepedia
{
    /// <summary>
    /// Example Text Editor Mod used to demonstrate the usage of the Rich HUD Framework.
    /// </summary>
    [MySessionComponentDescriptor(MyUpdateOrder.AfterSimulation)]
    public class SpacepediaMain : MySessionComponentBase
    {
        public static Networking Networking = new Networking(38501,38502);
        public static Translation Translation;
        public static SpacepediaWindow SpacepediaWindow;
        
        private static SpacepediaIntro Intro;
        private static NewChangesAsk ChangelogAsk;

        public static Color LabelColor = new Color(53, 66, 75, 80);
        public static Color BorderColor = new Color(53, 66, 75);
        
        private RichTerminal Terminal;

        public const int MouseScrollOffset = 20;

        public override void Init(MyObjectBuilder_SessionComponent sessionComponent)
        {
            RichHudClient.Init("SpacePedia", HudInit, ClientReset);
            MyAPIGateway.Utilities.MessageEntered += OnMessageEntered;
            MyCubeBuilder.Static.OnActivated += OnBlockChanged;
            MyCubeBuilder.Static.OnBlockVariantChanged += OnBlockChanged;
        }

        private static bool TryOpenWikiByHoldingBlock(bool force)
        {
            try
            {
                var id = MyCubeBuilder.Static?.CurrentBlockDefinition?.Id;
                if (id == null) return false;
                
                foreach (var kv in XMLParse.BlockToPage)
                {
                    if (kv.Key.Matches(id.Value))
                    {
                        var TreePage = XMLParse.GetTreeByPageName(kv.Value.UniqueId);
                        if (TreePage != null && (force || TreePage.Value.Value.OpenOnBlockSelection))
                        {
                            SpacepediaWindow.OpenPage(TreePage.Value.Key, TreePage.Value.Value, force); 
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("Main", "TryOpenWikiByHoldingBlock: " + e);
                return false;
            }
        }

        private static void OnBlockChanged()
        {
            TryOpenWikiByHoldingBlock(false);
        }

        private static void OnMessageEntered(string messageText, ref bool sendToOthers)
        {
            try
            {
                var toLower = messageText.ToLower();
                if (toLower.StartsWith("!wiki"))
                {
                    return;
                }
            
                sendToOthers = false;

                var parts = messageText.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length < 2)
                {
                    return;
                }
  
                var TreePage = XMLParse.GetTreeByPageName(parts[1]);
                if (TreePage == null)
                {
                    Common.SendChatMessageToMe("No such page: " + messageText, "[MIG] SpacePedia", "Red");
                    return;
                }
            
                SpacepediaWindow.OpenPage(TreePage.Value.Key, TreePage.Value.Value, true);
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("Main", "OnMessageEntered: " + e);
            }
        }

        public override void BeforeStart()
        {
            Networking?.Register();
        }

        public override void LoadData()
        {
            base.LoadData();
            Hooks.Init();
        }
        
        protected override void UnloadData()
        {
            try
            {
                Networking?.Unregister();
                Networking = null;
                Terminal?.Close();
                Terminal = null;
                XMLParse.SaveSettings();
                MyAPIGateway.Utilities.MessageEntered -= OnMessageEntered;
                if (MyCubeBuilder.Static != null)
                {
                    MyCubeBuilder.Static.OnActivated -= OnBlockChanged;
                    MyCubeBuilder.Static.OnBlockVariantChanged -= OnBlockChanged;
                }
                Hooks.Close();
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("Main", "UnloadData: " + e);
            }
        }

        private void HudInit()
        {
            var Language = MyAPIGateway.Session.Config.Language;
            Translation = new Translation(Language);
            
            ChangelogAsk = new NewChangesAsk(HudMain.HighDpiRoot) {Visible = false};
            SpacepediaWindow = new SpacepediaWindow(HudMain.HighDpiRoot) {Visible = false};

            Networking.GotIntro += GotIntro;
            if (!MyAPIGateway.Session.IsServer) Networking.AskIntro(XMLParse.CultureName);
            else
            {
                XMLParse.Intro = XMLParse.GetIntroXML();
                Networking.ForceInvokeGotIntro();
            }
            
            Terminal = new RichTerminal();
        }

        private void GotIntro()
        {
            if (!XMLParse.Intro.isNew())
            {
                Intro = new SpacepediaIntro(HudMain.HighDpiRoot) {Visible = false};

                if (!XMLParse.History.isSiaRead(MyAPIGateway.Session.Name, XMLParse.Intro.CreateTime))
                {
                    if (!MyAPIGateway.Session.IsServer) Networking.PlayerAccepted(XMLParse.Intro.CreateTime);

                    var serverIa = new HistoryXML.ServerIA(MyAPIGateway.Session.Name, XMLParse.Intro.CreateTime, DateTime.Now);
                    if (!XMLParse.History.ServerIAList.Contains(serverIa))
                    {
                        XMLParse.History.ServerIAList.Add(serverIa);
                        XMLParse.UpdateH();
                    }
                    Intro.Visible = true;
                    HudMain.EnableCursor = true;
                }
            }
            ToggleLastChanges();
        }
        
        public static void SpacepediaToggle(object sender, EventArgs e)
        {
            if ((Intro != null && Intro.Visible) | ChangelogAsk.Visible  | MyAPIGateway.Gui.ChatEntryVisible) return;

            if (!SpacepediaWindow.Visible)
            {
                if (!TryOpenWikiByHoldingBlock(true))
                {
                    SpacepediaWindow.Visible = !SpacepediaWindow.Visible;
                    HudMain.EnableCursor = SpacepediaWindow.Visible;
                }
            }
            else
            {
                SpacepediaWindow.Visible = !SpacepediaWindow.Visible;
                HudMain.EnableCursor = SpacepediaWindow.Visible;
            }
        }
        public static void SpacepediaToggleEscape(object sender, EventArgs e)
        {
            if ((Intro != null && Intro.Visible) | ChangelogAsk.Visible) return;
            if(!SpacepediaWindow.Visible) return;
            SpacepediaWindow.Visible = !SpacepediaWindow.Visible;
            HudMain.EnableCursor = SpacepediaWindow.Visible;
        }

        public static void ToggleLastChanges()
        {
            if (!XMLParse.DifHistory | (Intro != null && Intro.Visible) | !XMLParse.History.isAnyUnread()) return;
            ChangelogAsk.Visible = true;
            HudMain.EnableCursor = true;
        }

        public override void Draw()
        {
            if (!RichHudClient.Registered) { }
        }

        private void ClientReset()
        {
            Networking.GotIntro -= GotIntro;
            SpacepediaWindow?.ClientReset();

            /* At this point, your client has been unregistered and all of 
            your framework members will stop working.

            This will be called in one of three cases:
            1) The game session is unloading.
            2) An unhandled exception has been thrown and caught on either the client
            or on master.
            3) RichHudClient.Reset() has been called manually.
            */
        }
        public static void ScanText(string input, string desc = null)
        {
            const int PARSE_MAX_COUNT = 20;
            const string m_ScanPattern = "GPS:([^:]{0,32}):([\\d\\.-]*):([\\d\\.-]*):([\\d\\.-]*):";
            const string m_ColorScanPattern = "GPS:([^:]{0,32}):([\\d\\.-]*):([\\d\\.-]*):([\\d\\.-]*):(#[A-Fa-f0-9]{6}(?:[A-Fa-f0-9]{2})?):";
            
            var num = 0;
            var flag = true;
            var matchCollection = Regex.Matches(input, m_ColorScanPattern);
            if (matchCollection.Count == 0)
            {
                matchCollection = Regex.Matches(input,m_ScanPattern);
                flag = false;
            }
            var color = new Color(117, 201, 241);
            foreach (Match match in matchCollection)
            {
                var str = match.Groups[1].Value;
                double x = 0;
                double y = 0;
                double z = 0;
                try
                {
                    x = Math.Round(double.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture), 2);
                    y = Math.Round(double.Parse(match.Groups[3].Value, CultureInfo.InvariantCulture), 2);
                    z = Math.Round(double.Parse(match.Groups[4].Value, CultureInfo.InvariantCulture), 2);
                    if (flag)
                        color = new ColorDefinitionRGBA(match.Groups[5].Value);
                }
                catch (Exception e)
                {
                    Common.SendChatMessageToMe(e.Message, "[MIG] Spacepedia");
                }

                var gps = MyAPIGateway.Session.GPS.Create(str,desc, new Vector3D(x, y, z), true);
                gps.GPSColor = color;
                MyAPIGateway.Session.GPS.AddLocalGps(gps);
                ++num;
                if (num == PARSE_MAX_COUNT)
                    break;
            }
        }
    }
}
