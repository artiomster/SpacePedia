using System;
using System.Collections.Generic;
using System.Linq;
using Sandbox.Game;
using Sandbox.ModAPI;
using Scripts.Specials.Messaging;
using SharedLib;
using Spacepedia.XMLClasses;
using VRage;
using VRage.Game.Components;
using VRage.Utils;
using static Spacepedia.XMLClasses.HistoryXML;


namespace Spacepedia
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation)]
    public class XMLParse : MySessionComponentBase
    {
        private const string IntroFileName = "Intro";
        private const string WikiFileName = "Wiki";
        private const string ChangeLogsFileName = "ChangeLogs";
        private const string HistoryFileName = "History.xml";
        private const string ListOfAcceptedFileName = "ListOfAccepted.xml";
        private const string StylePresetsName = "WikiStyleSheets";

        private static readonly Dictionary<string, IntroXML> Intros = new Dictionary<string,IntroXML>();
        public static IntroXML Intro = new IntroXML();
        public static HistoryXML History = new HistoryXML();
        public static AcceptedXML ListOfAccepted = new AcceptedXML();
        public static readonly Dictionary<ulong, StylePresetsXML> StyleModePair = new Dictionary<ulong, StylePresetsXML>();
        public static SettingsXML settings = new SettingsXML();

        public static readonly Dictionary<string, ChangeLogsXML> AllModsChanges = new Dictionary<string, ChangeLogsXML>();
        public static readonly Dictionary<Tree, List<Page>> WikiPages = new Dictionary<Tree, List<Page>>();
        public static readonly Dictionary<BlockIdMatcher, Page> BlockToPage = new Dictionary<BlockIdMatcher, Page>();
        
        public static bool DifHistory;
        public static string CultureName = "";

        private static readonly bool ToLoadSettings = false; 

        public override void LoadData()
        {
            var languageDescription = MyTexts.Languages.Where(x => x.Key == MyAPIGateway.Session.Config.Language).Select(x => x.Value).FirstOrDefault();
            if (languageDescription != null) CultureName = string.IsNullOrWhiteSpace(languageDescription.CultureName) ? "" : languageDescription.CultureName;

            if (ToLoadSettings) ReadSettings(); 
            ReadMods("_" + CultureName);
            ReadIntro();
            ReadHistory();
            ReadAcceptances();
            ReadStylePresets();
        }
        
        public static KeyValuePair<Tree, Page>? GetTreeByPageName(string pageName)
        {
            foreach (var kv in WikiPages)
            {
                foreach (var page in kv.Value)
                {
                    if (page.UniqueId == pageName)
                    {
                        return new KeyValuePair<Tree, Page>(kv.Key, page);
                    }
                }
            }

            return null;
        }
        
        public static Tree GetTreeByName(string TreeName)
        {
            var Tree = WikiPages.Where(k => k.Key.TreeName == TreeName).Select(k => k.Key).FirstOrDefault();
            return Tree;
        }
        public static Tree GetTreeById(string TreeId)
        {
            var Tree = WikiPages.Where(k => k.Key.TreeId == TreeId).Select(k => k.Key).FirstOrDefault();
            return Tree;
        }
        public static Page GetPageByName(Tree Tree, string PageName)
        {
            if (!WikiPages.ContainsKey(Tree)) return null;
            var Page = WikiPages[Tree].Find(p => p.PageName == PageName);
            return Page;
        }
        public static Page GetPageById(Tree Tree, string PageId)
        {
            if (!WikiPages.ContainsKey(Tree)) return null;
            var Page = WikiPages[Tree].Find(p => p.UniqueId == PageId);
            return Page;
        }
        public static IntroXML GetIntroXML(string cultureName = null)
        {
            if (cultureName == null) cultureName = CultureName;
            if (Intros.ContainsKey(cultureName))
            {
                return Intros[cultureName];
            }
            if (Intros.ContainsKey("Default"))
            {
                return Intros["Default"];
            }
            return Intro;
        }

        private static void ReadIntro()
        {
            if (!MyAPIGateway.Session.IsServer) return;

            try
            {
                if (!MyAPIGateway.Utilities.FileExistsInWorldStorage(IntroFileName + ".xml", typeof(IntroXML)))
                {
                    using (var writer = MyAPIGateway.Utilities.WriteFileInWorldStorage(IntroFileName + ".xml", typeof(IntroXML)))
                    {
                        writer.Write(MyAPIGateway.Utilities.SerializeToXML(new IntroXML()));
                    }
                }

                foreach (var value in MyTexts.Languages.Values)
                {
                    var FileName = IntroFileName + "_" + value.CultureName + ".xml";
                    if (MyAPIGateway.Utilities.FileExistsInWorldStorage(FileName, typeof(IntroXML)))
                    {
                        using (var reader = MyAPIGateway.Utilities.ReadFileInWorldStorage(FileName, typeof(IntroXML)))
                        {
                            Intros.Add(value.CultureName, MyAPIGateway.Utilities.SerializeFromXML<IntroXML>(reader.ReadToEnd()));
                        }
                    }
                }
                
                if (MyAPIGateway.Utilities.FileExistsInWorldStorage(IntroFileName + ".xml", typeof(IntroXML)))
                {
                    using (var reader = MyAPIGateway.Utilities.ReadFileInWorldStorage(IntroFileName + ".xml", typeof(IntroXML)))
                    {
                        Intros.Add("Default", MyAPIGateway.Utilities.SerializeFromXML<IntroXML>(reader.ReadToEnd()));
                    }
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Intro File Read: " + e);
            }
        }
        
        private static void ReadStylePresets()
        {
            if (MyAPIGateway.Session.IsServer) return;
            try
            {
                foreach (var mod in MyAPIGateway.Session.Mods)
                {
                    if (MyAPIGateway.Utilities.FileExistsInModLocation($"Data/{StylePresetsName}.xml", mod))
                    {
                        using (var logReader = MyAPIGateway.Utilities.ReadFileInModLocation($"Data/{StylePresetsName}.xml", mod))
                        {
                            var StylePresets = MyAPIGateway.Utilities.SerializeFromXML<StylePresetsXML>(logReader.ReadToEnd());
                            StyleModePair.Add(mod.PublishedFileId, StylePresets);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "StylePresets File Read: " + e);
            }
        }

        public static void CreateExamples(XMLExample XML)
        {
            try
            {
                switch (XML)
                {
                    case XMLExample.Intro:
                    {
                        using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(IntroFileName + ".xml", typeof(IntroXML)))
                        {
                            writer.Write(MyAPIGateway.Utilities.SerializeToXML(new IntroXML()));
                        }
                        Common.SendChatMessageToMe("Added example to %AppData%/SpaceEngineers/Storage/*Spacepedia", "Spacepedia");
                        break;
                    }
                    case XMLExample.ChangeLogs:
                    {
                        using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(ChangeLogsFileName + ".xml", typeof(ChangeLogsXML)))
                        {
                            writer.Write(MyAPIGateway.Utilities.SerializeToXML(new ChangeLogsXML().New()));
                        }
                        Common.SendChatMessageToMe("Added example to %AppData%/SpaceEngineers/Storage/*Spacepedia", "Spacepedia");
                        break;
                    }
                    case XMLExample.Wiki:
                    {
                        using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(WikiFileName + ".xml", typeof(WikiXML)))
                        {
                            writer.Write(MyAPIGateway.Utilities.SerializeToXML(new WikiXML().New()));
                        }
                        Common.SendChatMessageToMe("Added example to %AppData%/SpaceEngineers/Storage/*Spacepedia", "Spacepedia");
                        break;
                    }
                    case XMLExample.StylePresets:
                    {
                        using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(StylePresetsName + ".xml", typeof(StylePresetsXML)))
                        {
                            writer.Write(MyAPIGateway.Utilities.SerializeToXML(new StylePresetsXML().New()));
                        }
                        Common.SendChatMessageToMe("Added example to %AppData%/SpaceEngineers/Storage/*Spacepedia", "Spacepedia");
                        break;
                    }
                }
                MyVisualScriptLogicProvider.ShowNotification(SpacepediaMain.Translation.ExampleCreated,2000);
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Example File Read/Write: " + e);
            }
        }

        private static void ReadMods(string cultureName)
        {
            foreach (var mod in MyAPIGateway.Session.Mods)
            {
                try
                {
                    if (MyAPIGateway.Utilities.FileExistsInModLocation($"Data/{ChangeLogsFileName}{cultureName}.xml", mod))
                    {
                        using (var logReader = MyAPIGateway.Utilities.ReadFileInModLocation($"Data/{ChangeLogsFileName}{cultureName}.xml", mod))
                        {
                            var changeLogs = MyAPIGateway.Utilities.SerializeFromXML<ChangeLogsXML>(logReader.ReadToEnd());
                            changeLogs.ModId = mod.PublishedFileId;
                            AllModsChanges.Add(mod.FriendlyName, changeLogs);
                        }
                    }
                    else if (MyAPIGateway.Utilities.FileExistsInModLocation(ChangeLogsFileName + ".xml", mod))
                    {
                        using (var logReader = MyAPIGateway.Utilities.ReadFileInModLocation(ChangeLogsFileName + ".xml", mod))
                        {
                            var changeLogs = MyAPIGateway.Utilities.SerializeFromXML<ChangeLogsXML>(logReader.ReadToEnd());
                            changeLogs.ModId = mod.PublishedFileId;
                            AllModsChanges.Add(mod.FriendlyName, changeLogs);
                        }
                    }
                }
                catch (Exception e)
                {
                    SpacepediaWindow.AddOrEditBugs("XMLParse", "ChangeLogs File Read in " + mod.FriendlyName + "("+ mod.PublishedFileId +"): " + e);
                }

                try
                {
                    if (MyAPIGateway.Utilities.FileExistsInModLocation($"Data/{WikiFileName}{cultureName}.xml", mod))
                    {
                        using (var reader = MyAPIGateway.Utilities.ReadFileInModLocation($"Data/{WikiFileName}{cultureName}.xml", mod))
                        {
                            var Wiki = MyAPIGateway.Utilities.SerializeFromXML<WikiXML>(reader.ReadToEnd());
                            foreach (var Tree in Wiki.Trees)
                            {
                                foreach (var Page in Tree.Pages)
                                {
                                    Page.ModId = mod.PublishedFileId;
                                }
                                if (WikiPages.ContainsKey(Tree))
                                    WikiPages[Tree].AddRange(Tree.Pages);
                                else
                                    WikiPages.Add(Tree, Tree.Pages);

                                foreach (var page in Tree.Pages)
                                {
                                    if (!string.IsNullOrEmpty(page.BindedBlock))
                                    {
                                        BlockToPage[new BlockIdMatcher(page.BindedBlock)] = page;
                                    }
                                }
                            }
                        }
                    }
                    else if (MyAPIGateway.Utilities.FileExistsInModLocation($"Data/{WikiFileName}.xml", mod))
                    {
                        using (var reader = MyAPIGateway.Utilities.ReadFileInModLocation($"Data/{WikiFileName}.xml", mod))
                        {
                            var Wiki = MyAPIGateway.Utilities.SerializeFromXML<WikiXML>(reader.ReadToEnd());
                            foreach (var Tree in Wiki.Trees)
                            {
                                foreach (var Page in Tree.Pages)
                                {
                                    Page.ModId = mod.PublishedFileId;
                                }
                                if (WikiPages.ContainsKey(Tree))
                                    WikiPages[Tree].AddRange(Tree.Pages);
                                else
                                    WikiPages.Add(Tree, Tree.Pages);
                                
                                foreach (var page in Tree.Pages)
                                {
                                    if (!string.IsNullOrEmpty(page.BindedBlock))
                                    {
                                        BlockToPage[new BlockIdMatcher(page.BindedBlock)] = page;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    SpacepediaWindow.AddOrEditBugs("XMLParse", "Wiki File Read in" + mod.FriendlyName + "("+ mod.PublishedFileId +"): " + e);
                }
            }
        }

        private static void ReadHistory()
        {
            //if (MyAPIGateway.Session.IsServer) return;

            if (!MyAPIGateway.Utilities.FileExistsInLocalStorage(HistoryFileName, typeof(HistoryXML)))
            {
                try
                {
                    using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage(HistoryFileName, typeof(HistoryXML)))
                    {
                        var newHistory = CreateNewHistory();
                        writer.Write(MyAPIGateway.Utilities.SerializeToXML(newHistory));
                    }
                }
                catch (Exception e)
                {
                    SpacepediaWindow.AddOrEditBugs("XMLParse", "History File Write: " + e);
                }
            }

            try
            {
                using (var HistoryReader = MyAPIGateway.Utilities.ReadFileInLocalStorage(HistoryFileName, typeof(HistoryXML)))
                {
                    History = MyAPIGateway.Utilities.SerializeFromXML<HistoryXML>(HistoryReader.ReadToEnd()) ?? new HistoryXML();
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "History File Read: " + e);
            }

            try
            {
                RenewHistory();
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Renew History: " + e);
            }
        }

        private static void ReadSettings()
        {
            try
            {
                if (MyAPIGateway.Utilities.FileExistsInLocalStorage("Settings.xml", typeof(SettingsXML)))
                {
                    using (var reader = MyAPIGateway.Utilities.ReadFileInLocalStorage("Settings.xml", typeof(SettingsXML)))
                    {
                        var Parsed = MyAPIGateway.Utilities.SerializeFromXML<SettingsXML>(reader.ReadToEnd());
                        if (Parsed != null) settings = Parsed;
                    }
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Settings File Read: " + e);
            }
        }
        public static BindsXML ReadBinds()
        {
            try
            {
                if (MyAPIGateway.Utilities.FileExistsInLocalStorage("Binds.xml", typeof(BindsXML)))
                {
                    using (var binds = MyAPIGateway.Utilities.ReadFileInLocalStorage("Binds.xml", typeof(BindsXML)))
                    {
                        return MyAPIGateway.Utilities.SerializeFromXML<BindsXML>(binds.ReadToEnd());
                    }
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Binds File Read: " + e);
            }
            return new BindsXML();
        }

        public static void SaveSettings()
        {
            try
            {
                using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage("Settings.xml", typeof(SettingsXML)))
                {
                    writer.Write(MyAPIGateway.Utilities.SerializeToXML(settings));
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Settings File Write: " + e);
            }
        }
        public static void SaveBinds(BindsXML cfg)
        {
            try
            {
                using (var writer = MyAPIGateway.Utilities.WriteFileInLocalStorage("Binds.xml", typeof(BindsXML)))
                {
                    writer.Write(MyAPIGateway.Utilities.SerializeToXML(cfg));
                }
            }
            catch (Exception e)
            {
                SpacepediaWindow.AddOrEditBugs("XMLParse", "Binds File Write: " + e);
            }
        }

        public static void UpdateH()
        {
            if (MyAPIGateway.Utilities.FileExistsInLocalStorage(HistoryFileName, typeof(HistoryXML)))
            {
                try
                {
                    using (var writer =
                        MyAPIGateway.Utilities.WriteFileInLocalStorage(HistoryFileName, typeof(HistoryXML)))
                    {
                        writer.Write(MyAPIGateway.Utilities.SerializeToXML(History));
                    }
                }
                catch (Exception e)
                {
                    SpacepediaWindow.AddOrEditBugs("XMLParse", "History File Update: " + e.Message);
                }
            }
        }

        public static void ReadAcceptances()
        {
            if (!MyAPIGateway.Session.IsServer) return;
            if (!MyAPIGateway.Utilities.FileExistsInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)))
            {
                try
                {
                    using (var writer = MyAPIGateway.Utilities.WriteFileInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)))
                    {
                        writer.Write(MyAPIGateway.Utilities.SerializeToXML(new AcceptedXML()));
                    }
                }
                catch (Exception e)
                {
                    MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia: XMLParse, Accepted File Create: " + e.Message);
                }
            }

            try
            {
                using (var AcceptedListReader = MyAPIGateway.Utilities.ReadFileInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)))
                {
                    ListOfAccepted = MyAPIGateway.Utilities.SerializeFromXML<AcceptedXML>(AcceptedListReader.ReadToEnd());
                    ListOfAccepted.CashedString = ListOfAccepted.ToString();
                }
            }
            catch (Exception e)
            {
                MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia: XMLParse, Accepted File Read: " + e.Message);
            }
        }

        public static void UpdateLOA(DateTime IntroCreateTime, ulong SteamId, DateTime date, bool isAccepted)
        {
            MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia UpdateLOA File exist: " + MyAPIGateway.Utilities.FileExistsInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)) + " / is New: " + GetIntroXML().isNew());
            if (!MyAPIGateway.Utilities.FileExistsInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)) ||  GetIntroXML().isNew()) return;
            try
            {
                MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia Accepted: " + SteamId + " / " + date + " / " + isAccepted);
                ListOfAccepted.Add(IntroCreateTime, SteamId, date, isAccepted);
                using (var writer = MyAPIGateway.Utilities.WriteFileInWorldStorage(ListOfAcceptedFileName, typeof(AcceptedXML)))
                {
                    writer.Write(MyAPIGateway.Utilities.SerializeToXML(ListOfAccepted));
                }
            }
            catch (Exception e)
            {
                MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia XMLParse Accepted File Update exception: " + e.Message);
            }
        }

        private static HistoryXML CreateNewHistory()
        {
            var history = new HistoryXML();

            foreach (var e in AllModsChanges)
            {
                var ModHistory = new List<Change>();

                foreach (var Changes in e.Value.Pages)
                {
                    ModHistory.Add(new Change(Changes.Header));
                }

                history.Changes.Add(new ChangesH(e.Key, ModHistory));
            }

            if (WikiPages != null)
            {
                foreach (var Tree in WikiPages)
                {
                    var WikiList = new List<WikiH.WikiP>();
                    foreach (var page in Tree.Value)
                    {
                        WikiList.Add(new WikiH.WikiP(page.UniqueId));
                    }

                    history.WikiHistory.Add(new WikiH(Tree.Key.TreeId, WikiList));
                }
            }

            return history;
        }

        private static void RenewHistory()
        {
            foreach (var modChanges in AllModsChanges)
            {
                var pages = modChanges.Value.Pages;
                var historyDic = History.Changes.ToDictionary(x => x.ModName, x => x.ChangesList);

                foreach (var y in pages)
                {
                    var V = new Change(y.UniqueId);
                    if (historyDic.ContainsKey(modChanges.Key))
                    {
                        if (historyDic[modChanges.Key].Contains(V)) continue;
                        historyDic[modChanges.Key].Add(V);
                        DifHistory = true;
                    }
                    else
                    {
                        historyDic.Add(modChanges.Key, new List<Change> {V});
                        DifHistory = true;
                    }
                }

                var NewHistoryList = historyDic.Select(x => new ChangesH(x.Key, x.Value)).ToList();
                History.Changes = NewHistoryList;
            }

            foreach (var Tree in WikiPages)
            {
                var a = new WikiH(Tree.Key.TreeId);
                if (History.WikiHistory.Contains(a))
                {
                    var i = History.WikiHistory.FindIndex(c => c.WikiTreeId == Tree.Key.TreeId);
                    foreach (var Page in Tree.Value)
                    {
                        if (History.WikiHistory[i].Pages.Contains(new WikiH.WikiP(Page.UniqueId))) continue;
                        History.WikiHistory[i].Pages.Add(new WikiH.WikiP(Page.UniqueId));
                        DifHistory = true;
                    }
                }
                else
                {
                    var WikiList = new List<WikiH.WikiP>();
                    foreach (var page in Tree.Value)
                    {
                        WikiList.Add(new WikiH.WikiP(page.UniqueId));
                    }

                    History.WikiHistory.Add(new WikiH(Tree.Key.TreeId, WikiList));
                    DifHistory = true;
                }
            }
            UpdateH();
        }
        
        public enum XMLExample
        {
            Intro,
            ChangeLogs,
            Wiki,
            StylePresets
        }
    }
}