﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using RichHudFramework.UI;
using RichHudFramework.UI.Rendering;
using Sandbox.Game;
using Sandbox.Game.Localization;
using Sandbox.ModAPI;
using VRage;
using VRage.Input;
using VRage.Utils;
using VRageMath;
using static RichHudFramework.Utils.Color;

namespace Spacepedia.XMLClasses
{
    public class Tree
    {
        public Tree() { }
        
        public Tree(IntroXML Intro)
        {
            TreeId = "0";
            TreeName = Intro.ListName;
        }
        
        public Tree(string TreeId, string TreeName)
        {
            this.TreeId = TreeId;
            this.TreeName = TreeName;
        }

        [XmlIgnore] private string HashCode => TreeId;
        [XmlAttribute("UniqueId")] public string TreeId { get; set; } = "TreeId";
        [XmlAttribute("Name")] public string TreeName = "TreeName";
        
        [XmlElement(typeof(PlainPage), ElementName = "PlainPage")] 
        [XmlElement(typeof(MeshPage), ElementName = "MeshPage")] 
        public List<Page> Pages;

        public override bool Equals(object o)
        {
            return TreeId == (o as Tree)?.TreeId;
        }

        public override int GetHashCode()
        {
            return HashCode.GetHashCode();
        }
    }
    
    public class Page
    {
        public Page(){}

        public Page(string uniqueId)
        {
            UniqueId = uniqueId;
        }
        
        [XmlIgnore] private string HashCode => UniqueId;
        [XmlIgnore] public ulong ModId;
        [XmlAttribute("UniqueId")] public string UniqueId { get; set; } = "PageId";
        [XmlAttribute] public bool OpenOnBlockSelection { get; set; }
        [XmlAttribute] public string BindedBlock { get; set; } = "";
        
        // ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
        [XmlIgnore] public virtual string PageName { get; set; } = "PageName";
        [XmlIgnore] public virtual string Header{ get; set; } = "";
        
        [XmlIgnore] public Color HeaderColor => ParseColor(HeaderColorString);
        [XmlIgnore] public Color TextColor => ParseColor(TextColorString);
        
        [XmlIgnore] public virtual string HeaderColorString { get; set; } = GetColorString(Color.White);
        [XmlIgnore] public virtual string TextColorString { get; set; } = GetColorString(Color.White);

        public override bool Equals(object obj)
        {
            return UniqueId == (obj as Page)?.UniqueId;
        }

        public override int GetHashCode()
        {
            return HashCode.GetHashCode();
        }
    }
    
    public class Cell
    {
        [XmlAttribute("WidthMult")] public float WidthMult;
        [XmlAttribute("Alignment")] public string AlignmentString;
        [XmlAttribute("Color")] public string ColorString = GetColorString(Color.White);
        [XmlAttribute("TextSize")] public float TextSize = 1f;
        [XmlAttribute("IsSmart")] public bool isSmart;
        
        [XmlIgnore] public TextAlignment Alignment => ParseAlignment<TextAlignment>() ?? TextAlignment.Left;
        [XmlIgnore] public Color TextColor => ParseColor(ColorString);
        
        protected T? ParseAlignment<T>() where T : struct
        {
            T a;
            if (!Enum.TryParse(AlignmentString, out a)) return null;
            return a;
        }
    }
    
    public class TextCell : Cell
    { 
        [XmlText] public string Text = "Text";
    }

    public class LinkCell : TextCell
    {
        [XmlAttribute("Url")] public string Url;
    }

    public class ButtonCell : LinkCell
    {
        [XmlIgnore] public new ParentAlignments Alignment => ParseAlignment<ParentAlignments>() ?? ParentAlignments.Center;

        [XmlAttribute("ButtonWidth")] public float ButtonWidth;
        [XmlAttribute("ButtonHeight")] public float ButtonHeight;
    }

    public class GpsLinkCell : TextCell
    {
        [XmlAttribute("GPS")] public string GPS;
        
        public void CreateGPS() => SpacepediaMain.ScanText(GPS, MyTexts.Get(MySpaceTexts.TerminalTab_GPS_NewFromClipboard_Desc).ToString());
    }
    
    public class GpsButtonCell : GpsLinkCell
    {
        [XmlIgnore] public new ParentAlignments Alignment => ParseAlignment<ParentAlignments>() ?? ParentAlignments.Center;

        [XmlAttribute("ButtonWidth")] public float ButtonWidth;
        [XmlAttribute("ButtonHeight")] public float ButtonHeight;
    }
    
    public class LinkToPageCell : Cell
    {
        [XmlAttribute("TreeId")] public string TreeId;
        [XmlAttribute("PageId")] public string PageId;
        [XmlText] public string Text = "Text";
    }

    public class ButtonToPageCell : LinkToPageCell
    {
        [XmlIgnore] public new ParentAlignments Alignment => ParseAlignment<ParentAlignments>() ?? ParentAlignments.Center;

        [XmlAttribute("ButtonWidth")] public float ButtonWidth;
        [XmlAttribute("ButtonHeight")] public float ButtonHeight;
    }

    public class ImageCell : Cell
    {
        [XmlIgnore] public Material ImageMaterial => new Material(Link, new Vector2(ImageWidth, ImageHeight));
        [XmlIgnore] public new ParentAlignments Alignment => ParseAlignment<ParentAlignments>() ?? ParentAlignments.Center;

        [XmlAttribute("ImageWidth")] public float ImageWidth;
        [XmlAttribute("ImageHeight")] public float ImageHeight;
        [XmlText] public string Link = "Link";
    }

    [XmlType("PlainPage")] public sealed class PlainPage : Page
    {
        public PlainPage() { }

        public PlainPage(IntroXML Intro)
        {
            PageName = Intro.PageName;
            Header = Intro.Header;
            Text = Intro.Text;
            TextSize = Intro.TextSize;
        }
        
        public PlainPage(ChangesPlainPage changes)
        {
            PageName = changes.PageName;
            Header = changes.Header;
            Text = changes.Text;
            TextSize = changes.TextSize;
        }
        
        public PlainPage(string PageName, string Text, float TextSize = 1f)
        {
            this.PageName = PageName;
            this.Text = Text;
            this.TextSize = TextSize;

        }
        public PlainPage(string uniqueId, string PageName, string Text, string TextColor = null, string HeaderColor = null)
        {
            this.UniqueId = uniqueId;
            this.PageName = PageName;
            this.Text = Text;
            this.HeaderColorString = HeaderColor ?? GetColorString(Color.White);
            this.TextColorString = TextColor ?? GetColorString(Color.White);
        }
            
        [XmlAttribute("Name")] public override string PageName { get; set; } = "PageName";
        [XmlAttribute("Header")] public override string Header{ get; set; } = "";
        [XmlAttribute("HeaderColor")] public override string HeaderColorString { get; set; } = GetColorString(Color.White);
        [XmlAttribute("TextColor")] public override string TextColorString { get; set; } = GetColorString(Color.White);
        [XmlAttribute("TextSize")] public float TextSize { get; set; } = 1f;
        [XmlAttribute("IsSmart")] public bool IsSmart { get; set; }
        [XmlElement("Text")] public string Text = "text";
        
        public override bool Equals(object obj)
        {
            var inPage = obj as PlainPage;
            if (inPage == null) return false;
            return PageName == inPage.PageName;
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
        
    [XmlType("MeshRows")] public sealed class MeshPage : Page
    {
        public MeshPage() {}
        
        public MeshPage(ChangesMeshPage changes)
        {
            PageName = changes.PageName;
            Header = changes.Header;
            MeshRows = changes.MeshRows;
        }
        
        [XmlAttribute("Name")] public override string PageName { get; set; } = "PageName";
        [XmlAttribute("Header")] public override string Header{ get; set; } = "";
        [XmlAttribute("HeaderColor")] public override string HeaderColorString { get; set; } = GetColorString(Color.White);

        [XmlElement("MeshRow")] public List<Row> MeshRows;
            
        [XmlType("Cells")] public class Row
        {
            [XmlElement(typeof(TextCell), ElementName = "TextCell")]
            [XmlElement(typeof(LinkCell), ElementName = "LinkCell")]
            [XmlElement(typeof(ButtonCell), ElementName = "ButtonCell")]
            [XmlElement(typeof(GpsLinkCell), ElementName = "GpsLinkCell")]
            [XmlElement(typeof(GpsButtonCell), ElementName = "GpsButtonCell")]            
            [XmlElement(typeof(LinkToPageCell), ElementName = "LinkToPageCell")]
            [XmlElement(typeof(ButtonToPageCell), ElementName = "ButtonToPageCell")]
            [XmlElement(typeof(ImageCell), ElementName = "ImageCell")]
            public List<Cell> Cells;
        }
    }
   
    [XmlType("PlainPage")] public sealed class ChangesPlainPage : Page
    {
        public ChangesPlainPage() { }
        
        public ChangesPlainPage(string PageName, string Text, float TextSize = 1f)
        {
            this.PageName = PageName;
            this.Text = Text;
            this.TextSize = TextSize;
        }
        
        [XmlAttribute("ShortName")] public override string PageName { get; set; } = "PageName";
        [XmlAttribute("LongName")] public override string Header{ get; set; } = "";
        [XmlElement("Text")] public string Text = "text";
        [XmlElement("TextSize")] public float TextSize = 1f;
    }
        
    [XmlType("MeshRows")] public class ChangesMeshPage : Page
    {
        [XmlAttribute("ShortName")] public override string PageName { get; set; } = "PageName";
        [XmlAttribute("LongName")] public override string Header{ get; set; } = "";
        [XmlElement("MeshRow")] public List<MeshPage.Row> MeshRows;
        
    }

    [Serializable] [XmlRoot("IntroXML")]
    public class IntroXML
    {
        [XmlElement("CreateTime")] public DateTime CreateTime = DateTime.MinValue;
        [XmlElement("ListName")] public string ListName = "ListName";
        [XmlElement("PageName")] public string PageName = "PageName";
        [XmlElement("Header")] public string Header = "Header";
        [XmlElement("Text")] public string Text = "New file";
        [XmlElement("TextSize")] public float TextSize = 1f;
        [XmlElement("CheckBoxText")] public string CheckBoxText = "I Agree";
        [XmlElement("CheckBoxRedText")] public string CheckBoxRedText = "Please read text to the end";
        [XmlElement("ButtonRedText")] public string ButtonRedText = "Please Accept";

        public void Update(SyncIntro data)
        {
            CreateTime = data.CreationTime;
            ListName = data.ListName;
            PageName = data.PageName;
            Header = data.Header;
            Text = data.Text;
            TextSize = data.TextSize;
            CheckBoxText = data.CheckBoxText;
            CheckBoxRedText = data.CheckBoxRedText;
            ButtonRedText = data.ButtonRedText;

            MyLog.Default.WriteLineAndConsole("[MIG] SpacePedia: got intro from server");
        }

        public bool isNew()
        {
            return CreateTime == new IntroXML().CreateTime;
        }

        public override string ToString()
        {
            return CreateTime + "\n" + ListName + "\n" + PageName + "\n" + Header + "\n" + Text + "\n" + CheckBoxText + "\n" +
                   CheckBoxRedText + "\n" + ButtonRedText;
        }
    }

    [Serializable]
    [XmlRoot("Origin")]
    public class ChangeLogsXML
    {
        [XmlIgnore] public ulong ModId;

        [XmlElement(typeof(ChangesPlainPage), ElementName = "PlainPageChanges")]
        [XmlElement(typeof(ChangesMeshPage), ElementName = "MeshPageChanges")]
        public List<Page> Pages;

        public ChangeLogsXML New()
        {
            return new ChangeLogsXML {
                Pages = new List<Page> {
                    new ChangesPlainPage(),
                    new ChangesMeshPage {
                        MeshRows = new List<MeshPage.Row> {
                            new MeshPage.Row {
                                Cells = new List<Cell> {
                                    new ImageCell
                                    {
                                        WidthMult = 1f/3f, ImageWidth = 200f, ImageHeight = 100f, Link = "ImageLink"
                                    },
                                    new TextCell {WidthMult = 2f/3f, Text = "Text", TextSize = 1.5f}
                                }
                            },
                            new MeshPage.Row {
                                Cells = new List<Cell> {
                                    new TextCell {WidthMult = 1f, Text = "Text"},
                                }
                            },
                            new MeshPage.Row {
                                Cells = new List<Cell> {
                                    new LinkCell {WidthMult = 1f, Url = "", AlignmentString = "Right",Text = "Text"},
                                }
                            },
                            new MeshPage.Row {
                                Cells = new List<Cell> {
                                    new TextCell {WidthMult = 1f/3f, Text = "Text"},
                                    new ButtonCell {WidthMult = 1f/3f, ButtonHeight = 40, ButtonWidth = 80, Url = "", Text = "Text"},
                                    new TextCell {WidthMult = 1f/3f, Text = "Text"},
                                }
                            },
                        }
                    }
                }
            };
        }
    }

    [Serializable] [XmlRoot("History")]
    public class HistoryXML : IEquatable<HistoryXML>
    {
        [XmlArrayItem("Wiki")] public List<WikiH> WikiHistory;
        [XmlArrayItem("Mod")] public List<ChangesH> Changes;
        [XmlArrayItem("Server")] public List<ServerIA> ServerIAList;

        public HistoryXML()
        {
            WikiHistory = new List<WikiH>();
            Changes = new List<ChangesH>();
            ServerIAList = new List<ServerIA>();
        }

        public void SetWikiPRead(string TreeId, string PageId, bool isRead = false)
        {
            foreach (var Tree in WikiHistory)
            {
                if (Tree.WikiTreeId != TreeId) continue;
                foreach (var Page in Tree.Pages)
                {
                    if (Page.PageId == PageId) Page.isRead = isRead;
                }
            }
        }

        public bool isWikiPRead(string TreeId, string PageId)
        {
            foreach (var Tree in WikiHistory)
            {
                if (Tree.WikiTreeId != TreeId) continue;
                foreach (var Page in Tree.Pages)
                {
                    if (Page.PageId == PageId) return Page.isRead;
                }
            }

            return false;
        }

        public void SetChangesRead(string ModName, string PageName, bool isRead = false)
        {
            var versionId = XMLParse.AllModsChanges[ModName]?.Pages.Find(p => p.PageName == PageName)?.UniqueId;
            
            foreach (var mod in Changes)
            {
                if (mod.ModName != ModName) continue;
                foreach (var Change in mod.ChangesList)
                {
                    if (Change.PageId == versionId) Change.isRead = isRead;
                }
            }
        }

        public bool isChangesRead(string ModName, string versionId)
        {
            foreach (var mod in Changes)
            {
                if (mod.ModName != ModName) continue;
                foreach (var Change in mod.ChangesList)
                {
                    if (Change.PageId == versionId) return Change.isRead;
                }
            }

            return false;
        }

        public bool isAllChangesRead(string ModName)
        {
            foreach (var mod in Changes)
            {
                if (mod.ModName != ModName) continue;
                foreach (var Change in mod.ChangesList)
                {
                    if (!XMLParse.AllModsChanges[ModName].Pages.Contains(new Page(Change.PageId))) continue;
                    if (Change.isRead == false) return false;
                }
            }
            return true;
        }

        public bool isAnyUnread()
        {
            foreach (var wiki in WikiHistory)
            {
                foreach (var Page in wiki.Pages)
                {
                    if (!Page.isRead) return true;
                }
            }

            foreach (var mod in Changes)
            {
                foreach (var Change in mod.ChangesList)
                {
                    if (!Change.isRead) return true;
                }
            }

            return false;
        }

        public void SetSiaRead(string ServerName, DateTime IntroDate, bool isAccepted)
        {
            if (ServerIAList != null)
            {
                foreach (var IA in ServerIAList)
                {
                    if (IA.Name == ServerName && IA.IntroDate == IntroDate) IA.isAccepted = isAccepted;
                }
            }
        }

        public bool isSiaRead(string ServerName, DateTime IntroDate)
        {
            if (ServerIAList != null)
            {
                foreach (var IA in ServerIAList)
                {
                    if (IA.Name == ServerName && IA.IntroDate == IntroDate) return IA.isAccepted;
                }
            }

            return false;
        }

        public override string ToString()
        {
            var sb = "";

            if (WikiHistory != null)
            {
                foreach (var Wiki in WikiHistory)
                {
                    sb += "[SP] ------------> " + Wiki.WikiTreeId + "\n";
                    foreach (var Page in Wiki.Pages) { sb += "[SP] ------------> " + Page.PageId + " / " + Page.isRead + "\n"; }
                    sb += "-------------//////////////////////-------------  + \n";
                }
            }

            if (Changes != null)
            {
                foreach (var mod in Changes)
                {
                    sb += "[SP] ------------> " + mod.ModName + "\n";
                    foreach (var chl in mod.ChangesList) { sb += "[SP] ------------> " + chl.PageId + "\n"; }
                    sb += "-------------//////////////////////-------------  + \n";
                }
            }

            if (ServerIAList != null)
            {
                foreach (var sia in ServerIAList){ sb += "[SP] ------------> " + sia.Name + " \\ " + sia.IntroDate + "\n";}
                sb += "-------------//////////END/////////-------------  + \n";
            }

            return sb;
        }

        public class ChangesH : IEquatable<ChangesH>
        {
            [XmlAttribute("Name")] public string ModName;
            [XmlArrayItem("Version")] public List<Change> ChangesList;

            public ChangesH(){}

            public ChangesH(string _ModName, List<Change> _ChangesList = null)
            {
                ModName = _ModName;
                ChangesList = _ChangesList ?? new List<Change>();
            }

            public bool Equals(ChangesH other)
            {
                return ModName == other?.ModName && ChangesList == other?.ChangesList;
            }
        }

        public class WikiH : IEquatable<WikiH>
        {
            [XmlAttribute("TreeId")] public string WikiTreeId;
            [XmlArrayItem("Page")] public List<WikiP> Pages;

            public WikiH(){}

            public WikiH(string _WikiTreeId, List<WikiP> _Pages = null)
            {
                WikiTreeId = _WikiTreeId;
                Pages = _Pages ?? new List<WikiP>();
            }

            public bool Equals(WikiH other)
            {
                return WikiTreeId == other?.WikiTreeId;
            }

            public class WikiP : IEquatable<WikiP>
            {
                [XmlAttribute("Id")] public string PageId;
                [XmlAttribute("isRead")] public bool isRead;

                public WikiP(){}

                public WikiP(string _PageId, bool _isRead = false)
                {
                    PageId = _PageId;
                    isRead = _isRead;
                }

                public bool Equals(WikiP other)
                {
                    return PageId == other?.PageId;
                }
            }
        }

        public class Change : IEquatable<Change>
        {
            [XmlAttribute("PageId")] public string PageId;
            [XmlAttribute("isRead")] public bool isRead;

            public Change() { }

            public Change(string pageId, bool _isRead = false)
            {
                PageId = pageId;
                isRead = _isRead;
            }

            public bool Equals(Change other)
            {
                return PageId == other?.PageId;
            }
        }

        public class ServerIA : IEquatable<ServerIA>
        {
            [XmlAttribute("Name")] public string Name;
            [XmlAttribute("IntroDateVersion")] public DateTime IntroDate;
            [XmlAttribute("Date")] public DateTime Data;
            [XmlAttribute("isAccepted")] public bool isAccepted;

            public ServerIA()
            {
            }

            public ServerIA(string Name, DateTime IntroDate, DateTime Data = new DateTime(), bool isAccepted = false)
            {
                this.Name = Name;
                this.IntroDate = IntroDate;
                this.Data = Data;
                this.isAccepted = isAccepted;
            }

            public bool Equals(ServerIA other)
            {
                return Name == other?.Name &&
                       IntroDate == other?.IntroDate;
            }
        }

        public bool Equals(HistoryXML other)
        {
            return WikiHistory == other?.WikiHistory &&
                   Changes == other?.Changes &&
                   ServerIAList == other?.ServerIAList;
        }
    }

    [Serializable] [XmlRoot("WikiXML")]
    public class WikiXML
    {
        [XmlArrayItem("Tree")] public List<Tree> Trees;

        public WikiXML New() {
            return new WikiXML {Trees = new List<Tree> {
                new Tree {
                    Pages = new List<Page> {
                        new PlainPage {IsSmart = true, Text = "[h1]text1[/h1] text [h2]text2[/h2] text [h3]text3[/h3]"},
                        new MeshPage {
                            MeshRows = new List<MeshPage.Row> {
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new ImageCell {WidthMult = 1f/3f, ImageWidth = 200f, ImageHeight = 100f, AlignmentString = (ParentAlignments.Top | ParentAlignments.Left).ToString(), Link = "example"},
                                        new TextCell {WidthMult = 2f/3f, Text = "Text", TextSize = 3f, ColorString = GetColorString(Color.Gold)}
                                    }
                                },
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new TextCell {WidthMult = 1f, Text = "Text"},
                                    }
                                },
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new LinkCell {WidthMult = 0.5f, Url = "https://youtube.com/", AlignmentString = "Right",Text = "https://youtube.com/", ColorString = GetColorString(Color.HotPink)},
                                        new ButtonCell {WidthMult = 0.5f, Url = "https://youtube.com/", AlignmentString = "Right",Text = "https://youtube.com/", ButtonWidth=300, ButtonHeight=15, ColorString = GetColorString(Color.HotPink)},
                                    }
                                },
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new GpsLinkCell {WidthMult = 0.5f, GPS = "GPS:Test #1:1025440.65:316290.62:784445.16:#FF75C9F1:", AlignmentString = "Right", Text = "GPS Test", ColorString = GetColorString(Color.HotPink)},
                                        new GpsButtonCell {WidthMult = 0.5f, GPS = "GPS:Test #1:1025440.65:316290.62:784445.16:#FF75C9F1:", AlignmentString = "Right", Text = "GPS Test", ButtonWidth=300, ButtonHeight=15, ColorString = GetColorString(Color.HotPink)},
                                    }
                                },
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new ButtonToPageCell{WidthMult = 1f, TreeId = "OtherTree", PageId = "PageId2", AlignmentString = "Center", Text = "OtherPage", ButtonWidth=300, ButtonHeight=15, ColorString = GetColorString(Color.Silver)},
                                    }
                                },
                                new MeshPage.Row {
                                    Cells = new List<Cell> {
                                        new TextCell {WidthMult = 1f/3f, Text = "Text", TextSize = 0.5f},
                                        //new ButtonCell {WidthMult = 1f/3f, ButtonHeight = 40, ButtonWidth = 80, Url = "https://steamcommunity.com/app/244850", AlignmentString = "Center", Text = "Link"},
                                        new LinkToPageCell{WidthMult = 1f/3f, TreeId = "OtherTree", PageId = "PageId2", AlignmentString = "Center", Text = "OtherPage", ColorString = GetColorString(Color.Silver)},
                                        new TextCell {WidthMult = 1f/3f, AlignmentString = "Right", Text = "Text"},
                                    }}}}}},
                new Tree("OtherTree","NewTree") {
                    Pages = new List<Page> {
                        new PlainPage("PageId2","New Page","text")}}}}; }
    }
    
    [Serializable] [XmlRoot("Root")] 
    public class AcceptedXML
    {
        [XmlArrayItem("Player")] public List<IdList> IntroList = new List<IdList>();
        [XmlIgnore] public string CashedString = "";

        public void Add(DateTime IntroCreateTime, ulong steamId, DateTime date, bool isAccepted)
        {
            var IdentityId = MyAPIGateway.Players.TryGetIdentityId(steamId);
            var PlayerName = MyVisualScriptLogicProvider.GetPlayersName(IdentityId);
            var id = new Id {PlayerName = PlayerName, steamId = steamId, dateTime = date, isAccepted = isAccepted};

            if (!IntroList.Contains(new IdList(IntroCreateTime))) IntroList.Add(new IdList(IntroCreateTime));

            foreach (var Intro in IntroList.Where(Intro => Intro.IntroCreateTime == IntroCreateTime))
            {
                if (Intro.PlayersList.Contains(id))
                {
                    foreach (var Player in Intro.PlayersList.Where(Player =>
                        Player.PlayerName == PlayerName && Player.steamId == steamId))
                    {
                        Player.dateTime = date;
                        Player.isAccepted = isAccepted;
                    }
                }
                else Intro.PlayersList.Add(id);
            }

            CashedString = ToString();
        }

        public override string ToString()
        {
            var str = "";
            foreach (var Intro in IntroList)
            {
                str += "Intro Date: " + Intro.IntroCreateTime + "\n";
                foreach (var Player in Intro.PlayersList)
                {
                    str += "\t\u2022 Player Name: " + Player.PlayerName +
                           " | SteamId: " + Player.steamId +
                           " | Date: " + Player.dateTime +
                           " | Accepted: " + Player.isAccepted +
                           "\n";
                }
            }

            return str;
        }

        public class IdList : IEquatable<IdList>
        {
            [XmlAttribute("IntroCreateTime")] public DateTime IntroCreateTime;
            [XmlArrayItem("Player")] public List<Id> PlayersList;

            public IdList() { }

            public IdList(DateTime IntroCreateTime)
            {
                this.IntroCreateTime = IntroCreateTime;
                PlayersList = new List<Id>();
            }

            public bool Equals(IdList other)
            {
                return IntroCreateTime == other?.IntroCreateTime;
            }
        }

        public class Id : IEquatable<Id>
        {
            [XmlAttribute("Name")] public string PlayerName;
            [XmlAttribute("SteamId")] public ulong steamId;
            [XmlAttribute("DateTime")] public DateTime dateTime;
            [XmlAttribute("isAccepted")] public bool isAccepted;

            public Id() { }

            public Id(string PlayerName, ulong steamId, DateTime dateTime, bool isAccepted)
            {
                this.PlayerName = PlayerName;
                this.steamId = steamId;
                this.dateTime = dateTime;
                this.isAccepted = isAccepted;
            }

            public bool Equals(Id other)
            {
                return PlayerName == other?.PlayerName && steamId == other?.steamId;
            }
        }
    }

    [Serializable] [XmlRoot, XmlType(TypeName = "SpacepediaBinds")]
    public class BindsXML
    {
        public static BindDefinition[] DefaultMain => defaultMain.Clone() as BindDefinition[];

        [XmlArray("ModifierGroup")] public BindDefinition[] mainGroup = DefaultMain;
        
        private static readonly BindDefinition[] defaultMain = new BindGroupInitializer {{"SpacepediaToggle", MyKeys.OemQuotes}}.GetBindDefinitions();
    }

    [Serializable] [XmlRoot, XmlType(TypeName = "SpacepediaSettings")]
    public class SettingsXML
    {
        [XmlElement("Transparency")] public Transparency transparency = new Transparency();
        [XmlElement("WindowSet")] public WindowSet dimension = new WindowSet();
        
        public class Transparency
        {
            [XmlAttribute("Auto")] public bool Auto = true;
            [XmlText] public float Percentage;
        }
        
        public class WindowSet
        {
            [XmlAttribute("Pos_x")] public float X;
            [XmlAttribute("Pos_Y")] public float Y;
            [XmlAttribute("MinWidth")] public float MinWidth = 1044f;
            [XmlAttribute("MinHeight")] public float MinHeight = 500f;
            [XmlAttribute("Width")] public float Width = 1044f;
            [XmlAttribute("Height")] public float Height = 800f;
            
            [XmlIgnore] public Vector2 MinSize => new Vector2(MinWidth,MinHeight);
            [XmlIgnore] public Vector2 Size
            {
                get { return new Vector2(Width, Height); }
                set { Width = value.X; Height = value.Y; }
            }
            [XmlIgnore] public Vector2 PositionOffset
            {
                get { return new Vector2(X, Y); }
                set { X = value.X; Y = value.Y; }
            }
        }
        
        public override string ToString()
        {
            return $"Transparency: (Auto: {transparency.Auto} Percentage: {transparency.Percentage}) WindowSet: (MinSize: {dimension.MinSize} Size: {dimension.Size})";
        }
    }
}