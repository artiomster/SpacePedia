﻿﻿using RichHudFramework.UI;
using Spacepedia.XMLClasses;
using VRageMath;

namespace Spacepedia
{
   
    public sealed class SpacepediaPage : HudElementBase
    {
        public readonly string PageName;

        private RichText Header
        {
            set
            {
                if ((value == null || value.ToString() == "") && PageName != null) header.Text = PageName;
                else header.Text = value;
            }
        }

        private readonly Label header;
        private readonly TexturedBox headerDivider;
        private readonly HudElementBase Display;
        public RichText Text
        {
            get { return (Display as PlainPageDisplay)?.Text; }
            set
            {
                var display = Display as PlainPageDisplay;
                if(display != null) display.Text = value;
            }
        }

        public SpacepediaPage(Page inPage, HudParentBase parent = null) : base(parent)
        {
            header = new Label(this)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Center | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Width,
                Height = 24f,
                Format = new GlyphFormat(inPage.HeaderColor, TextAlignment.Center),
            };

            headerDivider = new TexturedBox(header)
            {
                ParentAlignment = ParentAlignments.Bottom,
                DimAlignment = DimAlignments.Width,
                Padding = new Vector2(0f,10f),
                Color = new Color(53, 66, 75),
                Height = 1f,
            };
            
            if (inPage is PlainPage) Display = new PlainPageDisplay(inPage,this);
            if (inPage is MeshPage) Display = new MeshPageDisplay(inPage,this);

            Display.ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Left | ParentAlignments.Inner;
            Display.DimAlignment = DimAlignments.Width;
            
            PageName = inPage.PageName;
            Header = inPage.Header;
            
            UseCursor = true;
            ShareCursor = true;
        }

        protected override void Layout()
        {
            headerDivider.Width = Width - Padding.X;
            Display.Height = Height - header.Height - headerDivider.Height - Padding.Y - 5f;
        }
    }
}