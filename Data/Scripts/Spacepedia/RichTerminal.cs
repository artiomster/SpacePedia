﻿using RichHudFramework.UI;
using RichHudFramework.UI.Client;
using Spacepedia.XMLClasses;

namespace Spacepedia
{
    public class RichTerminal
    {
        private Binds Binds = new Binds();
        private TextPage AcceptedPage;
        
        public RichTerminal()
        {
            Networking.GotListOfAccepted += UpdateVisual;
            
            Binds.LoadBinds();
            Binds.OpenClose.NewPressed += SpacepediaMain.SpacepediaToggle;
            Binds.Close.NewPressed += SpacepediaMain.SpacepediaToggleEscape;
            
            AddToRichHudTerminal();
        }

        public void Close()
        {
            Binds?.SaveBinds();
            Binds = null;
        }

        private void UpdateVisual()
        {
            AcceptedPage.Enabled = true;
            AcceptedPage.Text = Networking.ListOfAccepted;
        }

        private void AddToRichHudTerminal()
        {
            RichHudTerminal.Root.Enabled = true;

            AcceptedPage = new TextPage
            {
                Name = SpacepediaMain.Translation.ListOfAccepted,
                HeaderText = SpacepediaMain.Translation.ListOfAccepted,
                SubHeaderText = "",
                Text = "",
                Enabled = false
            };

            var bindsPage = new RebindPage
            {
                Name = SpacepediaMain.Translation.Binds,
                GroupContainer = {{Binds.MainGroup, BindsXML.DefaultMain}}
            };

            RichHudTerminal.Root.AddRange(new IModRootMember[]
            {
                new ControlPage {Name = SpacepediaMain.Translation.AdminMenu, CategoryContainer = {AcceptedListPage(),ExampleCreation()}},
                AcceptedPage,
                bindsPage
            });
        }

        private ControlCategory AcceptedListPage()
        {
            var GetUpdate = new TerminalButton {Name = SpacepediaMain.Translation.ListOfAccepted};
            GetUpdate.ControlChanged += (o, e) => Networking.AskListOfAccepted();
            
            var ResetSettings = new TerminalButton {Name = SpacepediaMain.Translation.ResetSettings};
            ResetSettings.ControlChanged += (o, e) => SpacepediaMain.SpacepediaWindow.ResetSettings();
            
            return new ControlCategory
            {
                HeaderText = SpacepediaMain.Translation.GetUpdate,
                SubheaderText = "",
                TileContainer = {new ControlTile {GetUpdate},new ControlTile {ResetSettings}}
            };
        }

        private ControlCategory ExampleCreation()
        {
            var CreateExampleIntro = new TerminalButton {Name = SpacepediaMain.Translation.Intro};
            CreateExampleIntro.ControlChanged += (o, e) => XMLParse.CreateExamples(XMLParse.XMLExample.Intro);
            
            var CreateExampleWiki = new TerminalButton {Name = SpacepediaMain.Translation.Wiki};
            CreateExampleWiki.ControlChanged += (o, e) => XMLParse.CreateExamples(XMLParse.XMLExample.Wiki);
            
            var CreateExampleChangeLogs = new TerminalButton {Name = SpacepediaMain.Translation.ChangeLogs};
            CreateExampleChangeLogs.ControlChanged += (o, e) => XMLParse.CreateExamples(XMLParse.XMLExample.ChangeLogs);
            
            var CreateStylePresets = new TerminalButton {Name = SpacepediaMain.Translation.Presets};
            CreateStylePresets.ControlChanged += (o, e) => XMLParse.CreateExamples(XMLParse.XMLExample.StylePresets);

            return new ControlCategory
            {
                HeaderText = SpacepediaMain.Translation.Examples,
                SubheaderText = SpacepediaMain.Translation.CreateRecreate,
                TileContainer = {
                    new ControlTile 
                    {
                        CreateExampleIntro,
                        CreateExampleWiki,
                        CreateExampleChangeLogs,
                        CreateStylePresets
                    },
                }
            };
        }
    }
}