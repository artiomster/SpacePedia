﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using RichHudFramework.UI;
using Spacepedia.XMLClasses;
using VRage;
using VRage.Utils;
using VRageMath;
using GlyphFormatMembers = VRage.MyTuple<byte, float, VRageMath.Vector2I, VRageMath.Color>;
using static RichHudFramework.Utils.Color;

namespace Spacepedia
{
    using RichStringMembers = MyTuple<StringBuilder, GlyphFormatMembers>;

    public class TextFormatting
    {
        public static RichText GetFormattedText(PlainPage Page) => GetFormattedText(Page.Text, Page.TextSize, Page.TextColor, Page.ModId);
        public static RichText GetFormattedText(TextCell cell, ulong ModId) => GetFormattedText(cell.Text, cell.TextSize, cell.TextColor, ModId);
        public static RichText GetFormattedText(LinkCell cell, ulong ModId) => GetFormattedText(cell.Text, cell.TextSize, cell.TextColor, ModId);
        public static RichText GetFormattedText(LinkToPageCell cell, ulong ModId) => GetFormattedText(cell.Text, cell.TextSize, cell.TextColor, ModId);
        
        private const string REGEX = "\\[H([^]]*)\\]([^]]*)\\[\\/H]|(.\\w*\\n*\\t*)";
        private const string REGEX2 = "(?: (\\w)=([^ ]+))";
        
        private static string ReplacePresets(string Text, ulong ModId)
        {
            if (!XMLParse.StyleModePair.ContainsKey(ModId)) return Text;
            
            var updText = Text;
            
            foreach (var Preset in XMLParse.StyleModePair[ModId].Presets)
            {
                var settings = $"[H S={Preset.FontSize} F={Preset.Font} U={Preset.Underline} C={Preset.TextColorString}]";
                updText = updText.Replace($"[{Preset.PresetName}]", settings).Replace($"[/{Preset.PresetName}]","[/H]");
            }
            return updText;
        }

        private static RichText GetFormattedText(string Text, float TextSize, Color TextColor, ulong ModId)
        {
            var updText = ReplacePresets(Text, ModId);
            var apiData = new List<RichStringMembers>();
            var TextGroups = Regex.Matches(updText, REGEX);

            foreach (Match TGroup in TextGroups)
            {
                if (TGroup.Groups[1].Success)
                {
                    var StringSettings = Regex.Matches(TGroup.Groups[1].Value, REGEX2);
                    var _TextSize = 1f;
                    var Font = 0;
                    var Underline = 0;
                    var _TextColor = Color.White;
                    foreach (Match Set in StringSettings)
                    {
                        if (Set.Groups[1].Value == "S") float.TryParse(Set.Groups[2].Value, out _TextSize);
                        if (Set.Groups[1].Value == "F") int.TryParse(Set.Groups[2].Value, out Font);
                        if (Set.Groups[1].Value == "U") int.TryParse(Set.Groups[2].Value, out Underline);
                        if (Set.Groups[1].Value == "C") _TextColor = ParseColor(Set.Groups[2].Value);
                    }
                    apiData.Add(new RichStringMembers(new StringBuilder(TGroup.Groups[2].Value), new GlyphFormatMembers(0, _TextSize, new Vector2I(Font, Underline == 1 ? 4 : 0), _TextColor)));
                }
                else
                {
                    apiData.Add(new RichStringMembers(new StringBuilder(TGroup.Groups[3].Value), new GlyphFormatMembers(0, TextSize, new Vector2I(0, 0), TextColor)));
                }
            }
            return new RichText(apiData);
        }
    }

    [Serializable] [XmlRoot("StylePresets")]
    public class StylePresetsXML
    {
        [XmlElement] public List<StylePreset> Presets;

        public StylePresetsXML New()
        {
            return new StylePresetsXML {Presets = new List<StylePreset>
            {
                new StylePreset{PresetName = "h1", FontSize = 3, Font = 0, Underline = 0, TextColorString = GetColorString(Color.White)},
                new StylePreset{PresetName = "p2", FontSize = 2, Font = 0, Underline = 0, TextColorString = GetColorString(Color.White)},
                new StylePreset{PresetName = "R3", FontSize = 1, Font = 0, Underline = 1, TextColorString = GetColorString(Color.Red)}
            }};
        }
        
        public class StylePreset
        {
            [XmlText] public string PresetName { get; set; } = "h1";
            
            [XmlAttribute] public float FontSize { get; set; } = 1;
            [XmlAttribute] public int Font { get; set; }
            [XmlAttribute] public int Underline { get; set; }
            [XmlAttribute] public string TextColorString { get; set; } = GetColorString(Color.White);

            [XmlIgnore] public Color TextColor => ParseColor(TextColorString);
        }
    }
}