using System;
using RichHudFramework.UI;
using RichHudFramework.UI.Rendering;
using Sandbox.Game;
using Spacepedia.XMLClasses;
using VRageMath;
using EventHandler = RichHudFramework.EventHandler;

namespace Spacepedia
{
    public class ChangeLogPage : HudElementBase
    {
        public readonly ChangeLogList _ChangeLogList;
        private string SelectedName => _ChangeLogList.SelectedName;
        private HudElementBase SelectedInPage => _ChangeLogList.SelectedInPage;
        private HudElementBase lastPage;
        private readonly HudChain bodyChain;
        private readonly string ModName;
        
        public event EventHandler HSCI;
        private readonly Label Header;
        private readonly LabelBoxButton SubHeader;
        private readonly BorderBox body;
        private readonly TexturedBox headerDivider, middleDivider;
        
        public ChangeLogPage(string _ModName, ChangeLogsXML changes, HudParentBase parent = null) : base(parent)
        {
            ModName = _ModName;
            
            Header = new Label(this)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Center | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Width,
                Height = 24f,
                Format = new GlyphFormat(Color.White, TextAlignment.Center)
            };

            SubHeader = new LabelBoxButton(Header)
            {
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Center | ParentAlignments.InnerH,
                DimAlignment = DimAlignments.Width,
                Height = 24f,
                Padding = new Vector2(0f,5f),
                Format = new GlyphFormat(Color.White, TextAlignment.Center, style: FontStyles.Underline),
                Text = GenerateUrl(changes.ModId),
                Color = Color.Transparent,
                HighlightColor = SpacepediaMain.BorderColor,
            };
            SubHeader.MouseInput.LeftClicked += (sender, args) => OpenModUrl(changes.ModId);

            headerDivider = new TexturedBox(SubHeader)
            {
                ParentAlignment = ParentAlignments.Bottom,
                DimAlignment = DimAlignments.Width,
                Padding = new Vector2(0f,10f),
                Color = new Color(53, 66, 75),
                Height = 1f,
            };
            
            middleDivider = new TexturedBox()
            {
                Color = new Color(53, 66, 75),
                DimAlignment = DimAlignments.Width,
                Height = 1f,
            };

            _ChangeLogList = new ChangeLogList(_ModName)
            {
                Height = 65f, 
                DimAlignment = DimAlignments.Width,
            };
            
            _ChangeLogList.SelectionChanged += (sender, args) => HandleSelectionChange();
            _ChangeLogList.Add(changes.Pages);
            
            
            body = new BorderBox(this)
            {
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Left | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Width,
                Color = Color.Transparent
            };

            bodyChain = new HudChain(true, body)
            {
                SizingMode = HudChainSizingModes.FitChainAlignAxis | HudChainSizingModes.ClampChainBoth,
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Both,
                Spacing = 10f,
                CollectionContainer = {middleDivider,_ChangeLogList},
            };
            
            UseCursor = true;
            ShareCursor = true;
        }

        private static void OpenModUrl(ulong WorkshopId)
        {
            if (WorkshopId == 0)
                MyVisualScriptLogicProvider.OpenSteamOverlayLocal("https://steamcommunity.com/app/244850/workshop/");
            else 
                MyVisualScriptLogicProvider.OpenSteamOverlayLocal("https://steamcommunity.com/sharedfiles/filedetails/?id=" + WorkshopId);
        }
        private static string GenerateUrl(ulong WorkshopId)
        {
            if (WorkshopId == 0) return "https://steamcommunity.com/app/244850/workshop/";
            return "https://steamcommunity.com/sharedfiles/filedetails/?id=" + WorkshopId;
        }
        
        protected override void Layout()
        {
            headerDivider.Width = Width - Padding.X;
            body.Height = Height - Header.Height - SubHeader.Height - headerDivider.Height - Padding.Y - 5f;
            _ChangeLogList.Width = Width;

            if (SelectedInPage == null) return;
            
            var pageElement = SelectedInPage;
            pageElement.Width = Width;
            pageElement.Height = body.Height - _ChangeLogList.Height - middleDivider.Height - bodyChain.Spacing * 2;
        }
        
        private void HandleSelectionChange()
        {
            if (lastPage != null) bodyChain.RemoveChild(lastPage);
            
            Header.Text = SelectedName;
            if (SelectedInPage != null) bodyChain.Insert(0, SelectedInPage);
            
            lastPage = SelectedInPage;
            
            if (XMLParse.History.isAllChangesRead(ModName)) HSCI?.Invoke(this, EventArgs.Empty);
        }
    }
}