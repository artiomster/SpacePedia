﻿using System.Collections.Generic;
using VRage;

namespace Spacepedia
{
    public class Translation
    {
        private readonly int selectedLanguage;
        private readonly Dictionary<string,int> LanguagesDict = new Dictionary<string, int>
        {
            {"English",0}, {"Russian",1}
        };

        public Translation(MyLanguagesEnum Language)
        {
            var LanguageString = Language.ToString();
            if(LanguagesDict.ContainsKey(LanguageString)) selectedLanguage = LanguagesDict[LanguageString];
        }

        public string Next => Buttons.Next[selectedLanguage];
        public string Previous => Buttons.Previous[selectedLanguage];
        public string Close => Buttons.Close[selectedLanguage];
        public string ListOfAccepted => Buttons.ListOfAccepted[selectedLanguage];
        public string Intro => Buttons.Intro[selectedLanguage];
        public string Wiki => Buttons.Wiki[selectedLanguage];
        public string ChangeLogs => Buttons.ChangeLogs[selectedLanguage];
        public string Presets => Buttons.Presets[selectedLanguage];
        public string AdminMenu => PageNames.AdminMenu[selectedLanguage];
        public string Binds => PageNames.Binds[selectedLanguage];
        public string GetUpdate => Other.GetUpdate[selectedLanguage];
        public string Examples => Other.Examples[selectedLanguage];
        public string CreateRecreate => Other.CreateRecreate[selectedLanguage];
        public string NotAllowed => Other.NotAllowed[selectedLanguage];
        public string ExampleCreated => Other.ExampleCreated[selectedLanguage];
        public string NewBugMessage => Other.NewBugMessage[selectedLanguage];
        public string TransparencyCheckBox => Other.TransparencyCheckBox[selectedLanguage];
        public string ResetSettings => Buttons.ResetSettings[selectedLanguage];
        
        
        private static class Buttons
        {
            public static readonly string[] Next = {"Next", "Следующий"};
            public static readonly string[] Previous = {"Previous", "Предыдущий"};
            public static readonly string[] Close = {"Close", "Закрыть"};
            public static readonly string[] ListOfAccepted = {"List of Accepted", "Список принятых"};
            public static readonly string[] Intro = {"Intro", "Вступительное окно"};
            public static readonly string[] Wiki = {"Wiki", "Вики"};
            public static readonly string[] ChangeLogs = {"ChangeLogs", "Журнал изменений"};
            public static readonly string[] Presets = {"Wiki Style Sheets", "Таблицы стилей Wiki"};
            public static readonly string[] ResetSettings = {"Reset Settings", "Сбросить настройки"};
        }

        private static class PageNames
        {
            public static readonly string[] AdminMenu = {"Admin Menu", "Меню администратора"};
            public static readonly string[] Binds = {"Binds", "Привязка кнопок"};
        }

        private static class Other
        {
            public static readonly string[] GetUpdate = {"Get/Update", "Получить/Обновить"};
            public static readonly string[] Examples = {"Examples", "Примеры"};
            public static readonly string[] CreateRecreate = {"Create/Recreate", "Создать/Воссоздать"};
            public static readonly string[] NotAllowed = {"You are not worthy", "Ты не достоин"};  
            public static readonly string[] ExampleCreated = {"Example created in local storage", "Пример создан в локальном хранилище"};
            public static readonly string[] NewBugMessage = {"New bug added to SpacePedia", "Добавлена новая ошибка в SpacePedia"};
            public static readonly string[] TransparencyCheckBox = {"Auto Transparency", "Авто Прозрачность"};
        }
    }
}