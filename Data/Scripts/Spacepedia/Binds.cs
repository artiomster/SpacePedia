﻿using RichHudFramework.UI;
using RichHudFramework.UI.Client;
using Spacepedia.XMLClasses;
using VRage.Input;


namespace Spacepedia
{
    public class Binds
    {
        public readonly IBindGroup MainGroup;
        private readonly IBindGroup SecondaryGroup;

        public IBind OpenClose, Close;

        private BindsXML Cfg
        {
            get { return new BindsXML
            {
                mainGroup = MainGroup.GetBindDefinitions()
            };}
            set
            {
                MainGroup.TryLoadBindData(value.mainGroup);
                UpdateBindProperties();
            }
        }

        public Binds()
        {
            MainGroup = BindManager.GetOrCreateGroup("Main");
            MainGroup.RegisterBinds(BindsXML.DefaultMain);
            
            SecondaryGroup = BindManager.GetOrCreateGroup("Secondary");
            SecondaryGroup.RegisterBinds(new BindGroupInitializer {{"SpacepediaToggleEscape", MyKeys.Escape}}.GetBindDefinitions());
            UpdateBindProperties();
        }

        private void UpdateBindProperties()
        {
            OpenClose = MainGroup["SpacepediaToggle"];
            Close = SecondaryGroup["SpacepediaToggleEscape"];
        }

        public void SaveBinds()
        {
            XMLParse.SaveBinds(Cfg);
        }

        public void LoadBinds()
        {
            Cfg = XMLParse.ReadBinds();
        }
    }
}