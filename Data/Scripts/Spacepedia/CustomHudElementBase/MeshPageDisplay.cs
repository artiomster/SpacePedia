﻿﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MIG.Shared.SE;
using RichHudFramework.UI;
using RichHudFramework.UI.Rendering;
using Sandbox.Game;
using Spacepedia.XMLClasses;
using VRageMath;

namespace Spacepedia
{
    public class MeshPageDisplay : MeshPageDisplayBase
    {
        public MeshPageDisplay(MeshPage page, HudParentBase parent = null) : base(page?.MeshRows, page, parent)
        { }
        public MeshPageDisplay(Page page, HudParentBase parent = null) : base((page as MeshPage)?.MeshRows, page, parent)
        { }
    }
    public class MeshPageChangesDisplay : MeshPageDisplayBase
    {
        public MeshPageChangesDisplay(Page page, HudParentBase parent = null) : base(new MeshPage(page as ChangesMeshPage).MeshRows, page, parent)
        { }
    }

    public class MeshPageDisplayBase : HudElementBase
    {
        private readonly TexturedBox Box;
        private readonly HudChain bodyChain;
        private readonly ScrollBar verticalScroll;
        private readonly List<KeyValuePair<HudElementBase, float>> linksToResize = new List<KeyValuePair<HudElementBase, float>>();

        protected MeshPageDisplayBase(IEnumerable<MeshPage.Row> TableRows, Page Page, HudParentBase parent = null) : base(parent)
        {
            if (TableRows == null) return;
            
            Box = new TexturedBox(this)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Height,
                Color = SpacepediaMain.LabelColor,
            };

            bodyChain = new HudChain(true, Box)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Width,
                Spacing = 6f,
            };
            
            foreach (var row in TableRows)
            {
                var rowChain = new HudChain(false)
                {
                    SizingMode = HudChainSizingModes.FitChainOffAxis,
                    ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.InnerH,
                    DimAlignment = DimAlignments.Width,
                };
                
                foreach (var cell in row.Cells)
                {
                    if (cell is ImageCell)
                    {
                        var _cell = (ImageCell) cell;
                        var box = new EmptyHudElement {ParentAlignment = _cell.Alignment};
                        var TextureBox = new TexturedBox
                        {
                            ParentAlignment = _cell.Alignment | ParentAlignments.Inner,
                            DimAlignment = DimAlignments.IgnorePadding,
                            Material = _cell.ImageMaterial,
                            Size = _cell.ImageMaterial.size,
                        };
                        box.Height = TextureBox.Height;
                        box.RegisterChild(TextureBox);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(box, cell.WidthMult));
                        rowChain.Add(box);
                    }
                    else if(cell is GpsButtonCell)
                    {
                        var _cell = (GpsButtonCell) cell;
                        var box = new EmptyHudElement{ParentAlignment = _cell.Alignment};
                        var Button = new BorderedButton
                        {
                            ParentAlignment = _cell.Alignment | ParentAlignments.InnerH,
                            DimAlignment = DimAlignments.None,
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Size = new Vector2(_cell.ButtonWidth,_cell.ButtonHeight),
                            Format = new GlyphFormat(_cell.TextColor, TextAlignment.Center),
                            Text = _cell.Text,
                        };
                        if (cell.isSmart) Button.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        Button.MouseInput.LeftClicked += (sender, args) => _cell.CreateGPS();
                        box.RegisterChild(Button);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(box, cell.WidthMult));
                        rowChain.Add(box);
                    }
                    else if(cell is ButtonCell)
                    {
                        var _cell = (ButtonCell) cell;
                        var box = new EmptyHudElement{ParentAlignment = _cell.Alignment};
                        var Button = new BorderedButton
                        {
                            ParentAlignment = _cell.Alignment | ParentAlignments.InnerH,
                            DimAlignment = DimAlignments.None,
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Size = new Vector2(_cell.ButtonWidth,_cell.ButtonHeight),
                            Format = new GlyphFormat(_cell.TextColor, TextAlignment.Center),
                            Text = _cell.Text,
                        };
                        if (cell.isSmart) Button.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        Button.MouseInput.LeftClicked += (sender, args) => OpenUrl(_cell.Url);
                        box.RegisterChild(Button);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(box, cell.WidthMult));
                        rowChain.Add(box);
                    }
                    else if(cell is GpsLinkCell)
                    {
                        var _cell = (GpsLinkCell) cell;
                        var LinkBox = new LabelBoxButton
                        {
                            ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.InnerH,
                            Format = new GlyphFormat(_cell.TextColor, _cell.Alignment,  _cell.TextSize, FontStyles.Underline),
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Text = _cell.Text,
                            Color = Color.Transparent,
                            HighlightColor = SpacepediaMain.BorderColor
                        };
                        if (cell.isSmart) LinkBox.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        LinkBox.MouseInput.LeftClicked += (sender, args) => _cell.CreateGPS();
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(LinkBox, cell.WidthMult));
                        rowChain.Add(LinkBox);
                    }
                    else if(cell is LinkCell)
                    {
                        var _cell = (LinkCell) cell;
                        var LinkBox = new LabelBoxButton
                        {
                            ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.InnerH,
                            Format = new GlyphFormat(_cell.TextColor, _cell.Alignment,  _cell.TextSize, FontStyles.Underline),
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Text = _cell.Text,
                            Color = Color.Transparent,
                            HighlightColor = SpacepediaMain.BorderColor
                        };
                        if (cell.isSmart) LinkBox.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        LinkBox.MouseInput.LeftClicked += (sender, args) => OpenUrl(_cell.Url);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(LinkBox, cell.WidthMult));
                        rowChain.Add(LinkBox);
                    }
                    else if(cell is ButtonToPageCell)
                    {
                        var _cell = (ButtonToPageCell) cell;
                        var box = new EmptyHudElement{ParentAlignment = _cell.Alignment};
                        var ButtonToPage = new BorderedButton
                        {
                            ParentAlignment = _cell.Alignment | ParentAlignments.InnerH,
                            DimAlignment = DimAlignments.None,
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Size = new Vector2(_cell.ButtonWidth,_cell.ButtonHeight),
                            Format = new GlyphFormat(_cell.TextColor, TextAlignment.Center),
                            Text = _cell.Text,
                        };
                        if (cell.isSmart) ButtonToPage.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        ButtonToPage.MouseInput.LeftClicked += (sender, args) => SpacepediaMain.SpacepediaWindow.OpenPage(_cell.TreeId, _cell.PageId, true);
                        box.RegisterChild(ButtonToPage);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(box, cell.WidthMult));
                        rowChain.Add(box);
                    }
                    else if(cell is LinkToPageCell)
                    {
                        var _cell = (LinkToPageCell) cell;
                        var LinkToPageBox = new LabelBoxButton
                        {
                            ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.InnerH,
                            Height = 24f,
                            Format = new GlyphFormat(_cell.TextColor, _cell.Alignment,  _cell.TextSize, FontStyles.Underline),
                            Text = _cell.Text,
                            AutoResize = false,
                            Color = Color.Transparent,
                            HighlightColor = SpacepediaMain.BorderColor
                        };
                        if (cell.isSmart) LinkToPageBox.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        LinkToPageBox.MouseInput.LeftClicked += (sender, args) => SpacepediaMain.SpacepediaWindow.OpenPage(_cell.TreeId, _cell.PageId, true);
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(LinkToPageBox, cell.WidthMult));
                        rowChain.Add(LinkToPageBox);
                    }
                    else if(cell is TextCell)
                    {
                        var _cell = (TextCell) cell;
                        var CellBox = new LabelBox
                        {
                            ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.InnerH,
                            BuilderMode = TextBuilderModes.Wrapped,
                            AutoResize = false,
                            Format = new GlyphFormat(_cell.TextColor, _cell.Alignment,  _cell.TextSize),
                            Color = Color.Transparent,
                            Text = _cell.Text,
                        };
                        if (cell.isSmart) CellBox.Text = TextFormatting.GetFormattedText(_cell, Page.ModId);
                        CellBox.Height = CellBox.TextBoard.TextSize.Y;
                        linksToResize.Add(new KeyValuePair<HudElementBase, float>(CellBox, cell.WidthMult));
                        rowChain.Add(CellBox);
                    }
                }
                bodyChain.Add(rowChain);
            }
            
            verticalScroll = new ScrollBar(this)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.InnerH,
                DimAlignment = DimAlignments.Height | DimAlignments.IgnorePadding,
                Offset = new Vector2(15f,0f),
                Vertical = true,
            };
            
            // ReSharper disable once UnusedVariable
            var scrollDivider = new TexturedBox(Box)
            {
                ParentAlignment = ParentAlignments.Right,
                DimAlignment = DimAlignments.Height,
                Padding = new Vector2(15f,15f),
                Color = SpacepediaMain.BorderColor,
                Width = 1f,
            };
            
            IsMasking = true;
            UseCursor = true;
            ShareCursor = true;
            
        }
        
        private static readonly Regex[] WWW_WHITELIST = {
            new Regex("^(http[s]{0,1}://){0,1}[^/]*youtube.com/.*", RegexOptions.IgnoreCase),
            new Regex("^(http[s]{0,1}://){0,1}[^/]*youtu.be/.*", RegexOptions.IgnoreCase),
            new Regex("^(http[s]{0,1}://){0,1}[^/]*steamcommunity.com/.*", RegexOptions.IgnoreCase),
            new Regex("^(http[s]{0,1}://){0,1}[^/]*forum[s]{0,1}.keenswh.com/.*", RegexOptions.IgnoreCase)
        };

        private static bool IsUrlWhitelisted(string wwwLink)
        {
            foreach (var regex in WWW_WHITELIST)
            {
                if (regex.IsMatch(wwwLink)) return true;
            }
            return false;
        }
        
        private static void OpenUrl(string url)
        {
            if(!IsUrlWhitelisted(url)) Log.ChatError(url + " is not in whitelist");
            MyVisualScriptLogicProvider.OpenSteamOverlayLocal(url);
        }
        
        protected override void Layout()
        {
            Box.Width = Width - verticalScroll.slide.Width - verticalScroll.Offset.X;
            verticalScroll.slide.SliderHeight = (Size.Y / bodyChain.Size.Y) * verticalScroll.Height;

            foreach (var link in linksToResize)
            {
                var box = link.Key as LabelBox;
                if (box != null)
                {
                    box.Size =  new Vector2(Box.Width * link.Value, box.TextBoard.TextSize.Y); 
                    continue;
                }
                link.Key.Width = Box.Width * link.Value;
            }
        }
        
        protected override void HandleInput(Vector2 cursorPos)
        {
            base.HandleInput(cursorPos);
            IMouseInput vertControl = verticalScroll.slide.MouseInput;
            verticalScroll.Max = Math.Max(0f, bodyChain.Size.Y - Size.Y);
            if (!vertControl.IsLeftClicked) verticalScroll.Current = bodyChain.Offset.Y;
            if (IsMousedOver || verticalScroll.IsMousedOver)
            {
                if (SharedBinds.MousewheelUp.IsPressed) verticalScroll.Current -= SpacepediaMain.MouseScrollOffset;
                else if (SharedBinds.MousewheelDown.IsPressed) verticalScroll.Current += SpacepediaMain.MouseScrollOffset;
            }
            bodyChain.Offset = new Vector2(0, verticalScroll.Current);
        }
    }
}