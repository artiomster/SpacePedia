﻿﻿﻿using System;
using VRageMath;
using RichHudFramework.UI.Rendering;
using RichHudFramework.UI;
using RichHudFramework;
using RichHudFramework.UI.Client;
using Sandbox.ModAPI;

 namespace Spacepedia
{
    /// <summary>
    /// Example Text Editor window
    /// </summary>
    public sealed class SpacepediaIntro : WindowBase
    {
        private readonly TexturedBox bottomDivider;
        private readonly BorderedButton closeButton;
        private readonly NamedCheckBox CheckBox;
        private bool IsBoxChecked => CheckBox.IsBoxChecked;
        private bool isTextRead;
        private readonly LabelBox LabelBox, RedText;
        private readonly ScrollBar verticalScroll; 
        //private static readonly Material closeButtonMat = new Material("RichHudCloseButton", new Vector2(32f));

        /// <summary>
        /// Initializes a new Text Editor window and registers it to the specified parent element.
        /// You can leave the parent null and use the parent element's register method if you prefer.
        /// </summary>
        public SpacepediaIntro(HudParentBase parent = null) : base(parent)
        {
            HeaderBuilder.Format = TerminalFormatting.HeaderFormat;
            HeaderBuilder.SetText(XMLParse.Intro.Header);

            BodyColor = new Color(37, 46, 53);
            BorderColor = new Color(84, 98, 107);

            Padding = new Vector2(80f, 40f);
            MinimumSize = new Vector2(1044f, 500f);

            Size = new Vector2(1044f, 850f);

            var topDivider = new TexturedBox(header)
            {
                ParentAlignment = ParentAlignments.Bottom,
                DimAlignment = DimAlignments.Width,
                Padding = new Vector2(80f, 0f),
                Color = new Color(53, 66, 75),
                Height = 1f,
            };

            closeButton = new BorderedButton(this)
            {
                Text = SpacepediaMain.Translation.Close,
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Right | ParentAlignments.Inner,
                FitToTextElement = true,
                Padding = new Vector2(90f, 30f),
            };
            closeButton.MouseInput.LeftClicked += HandleCloseButton;

            CheckBox = new NamedCheckBox(closeButton)
            {
                ParentAlignment = ParentAlignments.Center | ParentAlignments.Left | ParentAlignments.InnerV,
                Name = XMLParse.Intro.CheckBoxText,
                IsBoxChecked = false,
            };
            CheckBox.MouseInput.LeftClicked += (sender, args) => HandleCheckBox();

            LabelBox = new LabelBox(topDivider)
            {
                ParentAlignment = ParentAlignments.Top | ParentAlignments.Left | ParentAlignments.Inner,
                Padding = new Vector2(90f, 30f),
                AutoResize = false,
                BuilderMode = TextBuilderModes.Wrapped,
                Format = GlyphFormat.White,
                VertCenterText = false,
                Text = XMLParse.Intro.Text
            };

            verticalScroll = new ScrollBar(LabelBox)
            {
                ParentAlignment = ParentAlignments.Right | ParentAlignments.Inner,
                DimAlignment = DimAlignments.Height | DimAlignments.IgnorePadding,
                Vertical = true,
                Visible = true
            };

            bottomDivider = new TexturedBox(header)
            {
                ParentAlignment = ParentAlignments.Bottom,
                DimAlignment = DimAlignments.Width,
                Padding = new Vector2(80f, 0f),
                Color = new Color(53, 66, 75),
                Height = 1f
            };
            
            RedText = new LabelBox(bottomDivider)
            {
                ParentAlignment = ParentAlignments.Bottom | ParentAlignments.Center | ParentAlignments.InnerH,
                Padding = new Vector2(90f, 15f),
                BuilderMode = TextBuilderModes.Wrapped,
                AutoResize = false,
                Format = new GlyphFormat(Color.Red,TextAlignment.Center),
                VertCenterText = false,
                Text = "",
                Height = 35f,
            };
        }

        protected override void Layout()
        {
            base.Layout();
            
            header.Height = 50f;

            // Bound window offset to keep it from being moved off screen
            Vector2 min = new Vector2(HudMain.ScreenWidth, HudMain.ScreenHeight) / (HudMain.ResScale * -2f), max = -min;
            Offset = Vector2.Clamp(Offset, min, max);
            
            // Update color opacity
            BodyColor = TerminalFormatting.OuterSpace.SetAlphaPct(HudMain.UiBkOpacity);
            
            ITextBoard textBoard = LabelBox.TextBoard;
            verticalScroll.slide.SliderHeight = (textBoard.Size.Y / textBoard.TextSize.Y) * verticalScroll.Height;

            LabelBox.Height = Height - header.Height - closeButton.Height - RedText.Height + RedText.Padding.Y;
            LabelBox.Width = header.Width - verticalScroll.Width;
            LabelBox.Color = BodyColor;
            
            RedText.Width = LabelBox.Width;
            RedText.Color = BodyColor;
            
            bottomDivider.Offset = new Vector2(0f,-LabelBox.Size.Y);
            
            header.Color = BodyColor;
        }

        private void HandleCloseButton(object sender, EventArgs e)
        {
            if (!IsBoxChecked)
            {
                if(XMLParse.Intro.ButtonRedText != "") RedText.Text = XMLParse.Intro.ButtonRedText;
                return;
            }
            
            XMLParse.History.SetSiaRead(MyAPIGateway.Session.Name, XMLParse.Intro.CreateTime, true);
            SpacepediaMain.Networking.PlayerAccepted(XMLParse.Intro.CreateTime, true);
            XMLParse.UpdateH();
            Visible = false;
            HudMain.EnableCursor = false;
            SpacepediaMain.ToggleLastChanges();
        }

        private void HandleCheckBox()
        {
            if (isTextRead) return;
            RedText.Text = XMLParse.Intro.CheckBoxRedText;
            CheckBox.IsBoxChecked = false;
        }

        protected override void HandleInput(Vector2 cursorPos)
        {
            base.HandleInput(cursorPos);
            ITextBoard textBoard = LabelBox.TextBoard;
            IMouseInput vertControl = verticalScroll.slide.MouseInput;
            verticalScroll.Max = Math.Max(0f, textBoard .TextSize.Y - textBoard .Size.Y);
            if (verticalScroll.Current >= verticalScroll.Max) isTextRead = true;
            if (!vertControl.IsLeftClicked) verticalScroll.Current = textBoard .TextOffset.Y;
            textBoard.TextOffset = new Vector2(0, verticalScroll.Current);
        }
    }
}
